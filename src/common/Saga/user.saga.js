import { takeLatest, all, put } from 'redux-saga/effects';
import { userConstants } from '../Const/user.constants';
import { history } from '../../routers/History';
import { doPost } from '../API/BaseApi';
import Axios from 'axios';
import { isEmpty, isEqual } from 'lodash';

function* login({ user }) {
    console.log(user);
    // try{
    //     const res = yield Axios.post('http://10.65.48.142:8080/efast/api/v1/login',user);
    //     console.log(res);
    //     if(!isEmpty(res) && isEqual(res.data.status,userConstants.LOGIN_STATUS_SUCCESS ) ){
    //         const userLogined = res.data.responseData;
    //         yield put({type: userConstants.LOGIN_SUCCESS, user:userLogined });
    //         localStorage.setItem('user', JSON.stringify(userLogined));
    //         history.push('/');
    //     }else {
    //         console.log('LOGIN_FAILURE');
    //         yield put({type: userConstants.LOGIN_FAILURE, user });
    //     }
    // }catch(err){
    //     console.log('LOGIN_FAILURE');
    //     yield put({type: userConstants.LOGIN_FAILURE, user });
    // }
    

     

    if(user.userName ==='quy' && user.password === '123'){
        console.log('LOGIN_SUCCESS');
        user['roleId']='1';//maker
        user['hasRole']=['bhxh','tthd'];
        yield put({type: userConstants.LOGIN_SUCCESS, user });
        localStorage.setItem('user', JSON.stringify(user));
        history.push('/');
    }else if(user.userName ==='bach' && user.password === '123'){
        console.log('LOGIN_SUCCESS');
        user['roleId']='1';//approver
        user['hasRole']=['bhxh','tthd'];
        yield put({type: userConstants.LOGIN_SUCCESS, user });
        localStorage.setItem('user', JSON.stringify(user));
        history.push('/');
    }else {
        console.log('LOGIN_FAILURE');
        yield put({type: userConstants.LOGIN_FAILURE, user });
    }
    
}

function* getAllUser() {
    const users = [{
        id: 1,
        username :'quy',
        role: 'gdv',
        cifno: '1'
    },{
        id: 2,
        username : 'bach',
        role: 'ksv',
        cifno: '1'
    },{
        id: 3,
        username : 'tu',
        role: 'gdv',
        cifno: '2'
    },{
        id : 4,
        username : 'huy',
        role: 'ksv',
        cifno: '2'
    }];
    
    yield put ({type:userConstants.GETALL_SUCCESS, users});    
}

function* sagaLogin() {
    yield takeLatest(userConstants.LOGIN_REQUEST, login);
}

function* sagaGetAllUser() {
    yield takeLatest(userConstants.GETALL_REQUEST, getAllUser);
}

function* deleteUser({id}){
    console.log(id);
    yield put ({type: userConstants.DELETE_SUCCESS, id})
}

function* sagaDeleteUser(){
    yield takeLatest(userConstants.DELETE_REQUEST, deleteUser);
}

function* rootSaga(){
     yield all ([
        sagaLogin(),
        sagaGetAllUser(),
        sagaDeleteUser()
    ])
}

export default rootSaga;