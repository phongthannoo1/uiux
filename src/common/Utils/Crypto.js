import utf8 from 'utf8';
import CryptoJS from 'crypto-js';

const encrypt = (value) => {
    var encoded = CryptoJS.AES.encrypt(value, 'secret key 123').toString();
    return encoded;
}

const decrypt = (value) => {
    var bytes  = CryptoJS.AES.decrypt(value, 'secret key 123');
    var decoded = bytes.toString(CryptoJS.enc.Utf8);
    return decoded;
}

export {encrypt, decrypt};
