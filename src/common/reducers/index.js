import { combineReducers } from 'redux';
import productReducer from '../../components/Product/reducer';
import reselectReducer from '../../components/Reselect/reducer';
import {alert} from './alert.reducer';
import {authentication} from './authentication.reducer';
import {registration} from './registration.reducer';
import {users} from './users.reducer';
import languageProviderReducer from '../../components/LanguageProvider/reducer';
import socialInsuranceReducer from '../../components/SocialInsurance/SocialInsuranceReducer';
import paymentInvoiceReducer from '../../components/PaymentInvoice/PaymentInvoiceReducer';
import bulkPaymentReducer from '../../components/BulkPayment/BulkPaymentReducer';
import salaryPaymentReducer from '../../components/SalaryPayment/SalaryPaymentReducer';
import accountListReducer from '../../components/AccountList/AccountListReducer';

let listReducer = [];

// const appReducers = combineReducers({
//     productReducer,
//     alert,
//     authentication,
//     registration,
//     users
// });

export const registryReducer = ( name, reducer) =>{
    let rootReducer = {
        ...listReducer,
        [name] : reducer
    };
    listReducer = rootReducer;
}

registryReducer('productReducer',productReducer);
registryReducer('alert',alert);
registryReducer('authentication',authentication);
registryReducer('registration',registration);
registryReducer('users',users);
registryReducer('reselect',reselectReducer);
registryReducer('language',languageProviderReducer);
registryReducer('socialInsuranceReducer',socialInsuranceReducer);
registryReducer('paymentInvoiceReducer',paymentInvoiceReducer);
registryReducer('bulkPaymentReducer',bulkPaymentReducer);
registryReducer('salaryPaymentReducer',salaryPaymentReducer);
registryReducer('accountListReducer',accountListReducer);


const appReducers = combineReducers(
    listReducer
);

export default appReducers;
