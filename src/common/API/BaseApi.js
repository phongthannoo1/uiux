import URL from "./axiosHttp";

const headers = {
    'Content-type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('token'),
    'Customer-Number': '300113679',
    'Customer-Username': 'thu.nh',
    'accept':'application/json',
    'Access-Control-Allow-Origin':'*'
}

const doGet = (link, callback,config) => {
    return URL.get(link,config)
        .then(res => callback(res, null))
        .catch(e => callback(null, e));
}

const doPost = (link, data, callback, headers, config) => {
    if (headers !== null && typeof headers === 'object') {
        headers.forEach(c => {
            URL.defaults.headers.common[c.key] = c.value;
        });
    }
    return URL.post(link, data, config)
        .then(res => callback(res, null))
        .catch(e => callback(null, e));
}

const doPut = (link, data, callback,config) => {
    return URL.put(link, data,config)
        .then(res => callback(res, null))
        .catch(e => callback(null, e));
}

const doDelete = (link, callback,config) => {
    return URL.delete(link,config)
        .then(res => callback(res, null))
        .catch(e => callback(null, e));
}

export {
    doGet,
    doPost,
    doPut,
    doDelete
}

