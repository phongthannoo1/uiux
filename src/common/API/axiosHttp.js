import axios from "axios";

axios.defaults.baseURL = 'http://localhost:8092';
axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem("token");
axios.defaults.headers.common['Content-Type'] = 'application/json';

export default axios.create({
    // baseURL: "http://localhost:9090/api",
    baseURL: "http://localhost:8092"
});