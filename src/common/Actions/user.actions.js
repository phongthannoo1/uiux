import { userConstants } from '../Const/user.constants';
import { userService } from '../API/user.service';
import { alertActions } from './alert.actions';
import { history } from '../../routers/History';

export const userActions = {
    login,
    logout,
    register,
    getAll,
    delete: _delete,
    loginTest,
    requestLogin,
    requestGetAllUser,
    requestDelete
};

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        userService.login(username, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function loginTest(username, password) {
    console.log('loginTest');
    console.log(username);
    console.log(password);
    return dispatch => {
        console.log('loginTest: '+ username);
        dispatch(request({ username }));
        // if(username === 'quy' && password === '123'){
        //     dispatch(success({username,password}));
        //     localStorage.setItem('user', JSON.stringify({username,password}));
        //     history.push('/');
        // }else {
        //     dispatch(failure({error: 'loi'}));
        //     dispatch(alertActions.error({error: 'loi'}));
        // }
                    
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function requestLogin(user) { return { type: userConstants.LOGIN_REQUEST, user } }

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

function requestGetAllUser() { return { type: userConstants.GETALL_REQUEST } }

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}

function requestDelete(id) { return { type: userConstants.DELETE_REQUEST, id } }