import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import appReducers from "../reducers";
// import rootSaga from '../Saga/';
// import sagaLogin from '../common/Saga/user.saga';
import { createLogger } from 'redux-logger';

const reduxSagaMonitorOptions = {};
const sagaMiddleware = createSagaMiddleware(reduxSagaMonitorOptions);
const loggerMiddleware = createLogger();

const store = createStore(
    appReducers,
    applyMiddleware(sagaMiddleware,loggerMiddleware)
);

// sagaMiddleware.run(rootSaga);
// sagaMiddleware.run(sagaLogin);

export const runSaga = (saga) => {
    sagaMiddleware.run(saga);
}


export default store;