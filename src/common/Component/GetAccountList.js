import { useEffect, useState } from "react";
import {doPost} from "../API/BaseApi";
import { Axios } from 'axios';
import { FormattedNumber } from "react-intl";
import { encrypt } from "../Utils/Crypto";

const AccountList = 
    ({listAccount,account}) => {
       console.log('listAccount');
       console.log(listAccount);
    return (
        <>
            <option value="" selected={true}>- Chọn tài khoản -</option>
            {listAccount.map((account, index) =>
                <option key={account.accountNo} value={encrypt(account.accountNo) } currency={account.currency} branchid={account.branchId} selected={account===account.accountNo}>
                    {account.accountNo} - {account.currency} - {account.availableBalance} - <FormattedNumber value={account.availableBalance} />
                    
                         {account.aliasName && ' - '+account.aliasName}
                    
                </option>
            )}
        </>
    )
}

export {AccountList};