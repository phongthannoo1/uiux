import React, {Suspense, lazy, Fragment} from 'react';
import {Route} from 'react-router-dom';
import { PrivateRoute } from '../../../routers/PrivateRoute';

const Product = lazy(() => import('../../../components/Product'));
const LoginPage = lazy(() => import('../../../components/Login'));
const RegisterPage = lazy(() => import('../../../components/Register'));
const HomeIndex = lazy(() => import('../../../components/Home'));
const Reselect = lazy(() => import('./../../../components/Reselect'));
const AccessDeniedPage = lazy(() => import('./../AppDenied'));
const PaymentInvoice = lazy(() => import('./../../../components/PaymentInvoice'));
const SocialInsurance = lazy(() => import('./../../../components/SocialInsurance'));
const BulkPayment = lazy(() => import('./../../../components/BulkPayment'));
const SalaryPayment = lazy(() => import('./../../../components/SalaryPayment'));
const AccountList  = lazy(() => import('./../../../components/AccountList'));

const roles={
    bhxh : 'bhxh',
    tthd : 'tthd'
}

function AppMain (){
    return(
        <Fragment>
            {/* product */}

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute path="/product" roles={[roles.bhxh]} roleId={'1'} component={Product} />
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute exact path="/" component={HomeIndex} />
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute exact path="/reselect" component={Reselect} />
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute path="/payment-invoice" roles={[roles.tthd]} roleId={'1'} component={PaymentInvoice} />
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute path="/social-insurance" roles={[roles.bhxh]} roleId={'1'} component={SocialInsurance} />
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute path="/bulk-payment" roles={[roles.bhxh]} roleId={'1'} component={BulkPayment} />
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute path="/salary-payment" roles={[roles.bhxh]} roleId={'1'} component={SalaryPayment} />
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <PrivateRoute path="/account-list" roles={[roles.bhxh]} roleId={'1'} component={AccountList} />
            </Suspense>









            
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/login" component={LoginPage} />
            </Suspense>
            
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/register" component={RegisterPage} />
            </Suspense>
            
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <h6 className="mt-5">
                            Please wait while we load all the Components examples
                            <small>Because this is a demonstration we load at once all the Components examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/access-denied" component={AccessDeniedPage} />
            </Suspense>
            
            
            {/* <Redirect from="*" to="/" /> */}
        </Fragment>
    )
}

export {AppMain};