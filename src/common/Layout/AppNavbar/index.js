import {Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';



function NavBar(){
    return(
        <Nav defaultActiveKey="/" className="flex-column">
            <Link to="/">Home</Link>
            <Link to="/product">Product</Link>
            <Link to="/reselect">Reselect</Link>
            <Link to="/account-list">Tài khoản</Link>
            <Link to="/payment-invoice">Thanh toán hóa đơn</Link>            
            <Link to="/social-insurance">Bảo hiểm xã hội</Link>           
            <Link to="/bulk-payment">Chuyển tiền theo file</Link>        
            <Link to="/salary-payment">Chi lương online</Link>
            <Link to="/login">
                Logout
            </Link>
        </Nav>
    )
}

export {NavBar};