import { useState } from "react";
import { Button, Form, FormControl, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { changeLocale } from './../../../components/LanguageProvider/actions';

function AppHeader () {
    const [locale, setLocale] = useState("vi");
    const dispatch = useDispatch();
    const userName = useSelector(state=>state.authentication.user.userName);

    const handleChangeLocale = (value)=>{
        setLocale(value);
        dispatch(changeLocale(value));
    }

    return (
        <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">eFast</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#link">Link</Nav.Link>
            <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown>
            </Nav>
            <Form inline>
            <FormattedMessage id="efast.title"/>
            <FormattedMessage id="efast.user" values={{userName}} />
            <Nav.Link onClick={()=>handleChangeLocale('vi')}>Vi</Nav.Link>
            <Nav.Link onClick={()=>handleChangeLocale('en')}>En</Nav.Link>
            <Nav.Link onClick={()=>handleChangeLocale('ja')}>Ja</Nav.Link>
            <Nav.Link onClick={()=>handleChangeLocale('kr')}>Kr</Nav.Link>
            <Nav.Link onClick={()=>handleChangeLocale('ch')}>Ch</Nav.Link>

            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
            </Form>
        </Navbar.Collapse>
        </Navbar>
    );
}

export {AppHeader};