import { Fragment } from "react";
import { Link } from 'react-router-dom';


function AccessDeniedPage(){
    return(
        <Fragment>
                <h2>Not this time, access forbidden!</h2>
                <Link to="/" >Home</Link>
        </Fragment>
    )
}

export default AccessDeniedPage;