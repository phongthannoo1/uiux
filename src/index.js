import React from 'react';
import ReactDOM from 'react-dom';
import App from './App/App';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Provider } from 'react-redux';
import store from './common/Store/Store';
import { LanguageProvider } from './components/LanguageProvider';
import './assets/styles/addons.css';
import './assets/styles/style.css';
import './assets/shadowbox/shadowbox.css';
import './assets/styles/validationEngine.jquery.css';
import './assets/styles/customize.css';
import './assets/styles/jquery.smartbanner.css';
require('dotenv').config();

// const initialState = {};
// const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store}>
    <LanguageProvider>
      <App />
    </LanguageProvider>
  </Provider>,
  document.getElementById('root')
);
