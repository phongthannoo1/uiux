var dtCh= "/";
	var dtCh2 = ":";
	var minYear=1900;
	var maxYear=9999;

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

//loandt1 check date mm/yyyy
function isDate6(object){
	var dtStr = object.value;
	if(dtStr.length <= 0 ) {

		return false;
	}
	if (dtStr.length > 7){
		alert('Ngày báo cáo không đúng định dạng mm/yyyy');
		return false;
	}
	var daysInMonth = DaysArray(12);

	var pos2 = dtStr.indexOf(dtCh);

	var strMonth = dtStr.substring(0, pos2);

	var strYear = dtStr.substring(strMonth.length+1);

	strYr = strYear;

	if (strMonth.charAt(0)=="0" && strMonth.length > 1){
		strMonth=strMonth.substring(1);
	}
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1){
			strYr=strYr.substring(1);
		}
	}

	month = parseInt(strMonth);

	year = parseInt(strYr);

	if (pos2 == -1) {
		alert('Ngày báo cáo không đúng định dạng mm/yyyy');;
		return false;
	}
	if (isNaN(strMonth) || strMonth.length<1 || month<1 || month>12) {
		alert('Ngày báo cáo không đúng định dạng mm/yyyy');
		return false;
	}

	if (isNaN(strYr) || strYear.length != 4 || year==0) {
		alert('Ngày báo cáo không đúng định dạng mm/yyyy');
		return false;
	}

	return true;
}
//loandt1 chuyen tu common sang
function isDate8(object){
	var dtStr = object.value;
	if(dtStr.length <= 0) {
		//alert("Ban chua nhap du thong tin!");
		return false;
	}
	var daysInMonth = DaysArray(12);
	var pos1 = dtStr.indexOf(dtCh);
	var pos2 = dtStr.indexOf(dtCh, pos1 + 1);
	var strDay = dtStr.substring(0, pos1);
	var strMonth = dtStr.substring(pos1 + 1, pos2);
	var strYear = dtStr.substring(pos2 + 1, pos2 + 3);

	strYr = strYear;
	if (strDay.charAt(0) == "0" && strDay.length > 1){
		strDay = strDay.substring(1);
	}
	if (strMonth.charAt(0)=="0" && strMonth.length > 1){
		strMonth=strMonth.substring(1);
	}
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1){
			strYr=strYr.substring(1);
		}
	}

	month = parseInt(strMonth);
	day = parseInt(strDay);
	year = parseInt(strYr);

	if (pos1 == -1 || pos2 == -1) {
		//alert("Ngay chua dung dinh dang : dd/mm/yyyy");
		return false;
	}
	if (isNaN(strMonth) || strMonth.length<1 || month<1 || month>12) {
		//alert("Nhap thang sai!");
		return false;
	}
	if (isNaN(strDay) || strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
  		//alert("Nhap ngay sai!");
		return false;
	}
	if (isNaN(strYr) || strYear.length != 2 || year==0) {
		//alert("Nhap nam sai!");
		return false;
	}

	return true;
}
//loandt chuyen tu package khac sang, dung cho NSNN
function isTaxPrd(object){
	var dtStr = object.value;
	if(dtStr.length <= 0) {
		return false;
	}
	var pos1 = dtStr.indexOf(dtCh);
	var strMonth = dtStr.substring(0, pos1);
	var strYear = dtStr.substring(pos1 + 1, pos1 + 5);

	strYr = strYear;
	//if (strMonth.charAt(0) == "0" && strMonth.length > 1){
		//strMonth = strMonth.substring(1);
	//}
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1){
			strYr=strYr.substring(1);
		}
	}
	month = parseInt(strMonth, 10);
	year = parseInt(strYr, 10);
	if (pos1 == -1) {
		alert("Ky thue khong dung dinh dang : mm/yyyy");
		return false;
	}
	//alert("Nhung= " + strMonth + strYear);
	//alert("Nhung= " + month);
	if (isNaN(strMonth) || strMonth.length<1 || month<1 || month>12 || strMonth.length != 2 || isNaN(month) || strMonth.charAt(0) == " ") {
		alert("Ky thue: Nhap thang sai!");
		return false;
	}
	if (isNaN(strYr) || strYear.length != 4 || year==0 || isNaN(year)) {
		alert("Ky thue: Nhap nam sai!");
		return false;
	}

	return true;
}

function formatCurrency2(object) {
	num = object.value.toString().replace(/\$|\,/g,'');
	if (object.value == '') {
		num = 0;
	}
	if (Trim(num).length == 0){
		return false;
	}
	if(isNaN(num)) {
		alert(object.value + " la so tien nhap khong dung !");
		return false;
	}
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
	object.value = (num + '.' + cents);

	//Kiem tra phan le sau thap phan
	if (stringToDouble(cents) > 0) {
		alert("So tien khong hop le. Khong chap nhan phan le sau thap phan!");
		return false;
	}

	//Tinh tong so tien thuc nop
	getTotalAmt();

	return true;
}
//Function cho Thu NSNN
function getTotalAmt() {
	//alert("Tinh tong so tien thuc nop");
	//Tinh tong so tien thuc nop
	var listSize = document.getElementById("listSize").value;
	var amount = 0;
	var totalAmount = 0;
	var name = "";
//	var orgAmt = "";
//	var orgAmtVal = 0;
//	var amt = 0;
	for (var i = 0; i < listSize; i++) {
		name = "bingBong[" + i + "].taxAmt";
		//name2 = "bingBong[" + i + "].amt";
		amount = stringToDouble(Trim(document.getElementById(name).value));
		//document.getElementById(name2).value = amount;
		totalAmount += amount;
		//Kiem tra xem so tien nop vao <= so tien goc phai nop
		//orgAmt = "bingBong[" + i + "].orgTaxAmt";
		//orgAmtVal = stringToDouble(Trim(document.getElementById(orgAmt).value));

//		if (orgAmtVal < amount) {
//			alert("So tien thuc nop vuot qua so tien phai nop!");
//			document.getElementById(name).focus();
//			return false;
//		}
	}
	//alert("Tinh tong so tien thuc nop == " + totalAmount);
	document.getElementById("totalAmt").value = totalAmount;
	document.getElementById("strRealAmt").value = formatValueCurrency2(totalAmount);
	readNumber2Text(totalAmount);
	return true;
}



function stateChanged()
{
if (xmlhttp3.readyState==4)
  {
  document.getElementById("money2Text").innerHTML =  xmlhttp3.responseText;
  }
}

function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}
//Ham cu
//function getTotalAmt() {
//	//alert("Tinh tong so tien thuc nop");
//	//Tinh tong so tien thuc nop
//	var listSize = document.getElementById("listSize").value;
//	var amount = 0;
//	var totalAmount = 0;
//	var name = "";
//	var orgAmt = "";
//	var orgAmtVal = 0;
//	var amt = 0;
//	for (var i = 0; i < listSize; i++) {
//		name = "bingBong[" + i + "].taxAmt";
//		name2 = "bingBong[" + i + "].amt";
//		amount = stringToDouble(Trim(document.getElementById(name).value));
//		document.getElementById(name2).value = amount;
//		totalAmount += amount;
//		//Kiem tra xem so tien nop vao <= so tien goc phai nop
//		orgAmt = "bingBong[" + i + "].orgTaxAmt";
//		orgAmtVal = stringToDouble(Trim(document.getElementById(orgAmt).value));
//
//		if (orgAmtVal < amount) {
//			alert("So tien thuc nop vuot qua so tien phai nop!");
//			document.getElementById(name).focus();
//			return false;
//		}
//	}
//	//alert("Tinh tong so tien thuc nop == " + totalAmount);
//	document.getElementById("totalAmt").value = totalAmount;
//	document.getElementById("strRealAmt").value = formatValueCurrency2(totalAmount);
//	readNumber2Text(totalAmount);
//	return true;
//}

/**
 *
 * @param objVal
 * @return
 */
function stringToDouble(objVal) {
	try {
		var num = (objVal+'').replace(/\$|\,/g,'');
		return parseFloat(num);
	}
	catch(err) {
	     
	    	return 0;
	}
	 
}

/**
 *
 * @param object
 * @return
 */
function formatAmtAmount(object){
    num = object.value.toString().replace(/[^0-9.]/g, '');
    object.value = num;
    formatAmt(object);
}

function doInputData(object, type) {
    //var input = window.event.keyCode ? window.event.keyCode : window.event.which;
	var input = window.event.keyCode;
    var OK = true;
    if (type == 'number') {
        if (!(input >= 48 && input <= 57)) {
            OK = false;
        }
    } else if (type == 'currency') {
        if (!(input >= 48 && input <= 57 || input >= 43 && input <= 46)) {
            OK = false;
        }
    } else if (type == 'date') {
        if (!(input >= 47 && input <= 57)) {
            OK = false;
        }
    } else if (type == 'time') {
        if (!(input >= 48 && input <= 58)) {
            OK = false;
        }
    } else if (type == 'datetime') {
        if (!(input >= 47 && input <= 58)) {
            OK = false;
        }
    } else if (type == 'rate') {
        if (!( input == 46 || (input >= 48 && input <= 57))) {
            OK = false;
        }
    }
    if (!OK) {
        window.event ? event.keyCode = 0 : evt.which = 0;
    }
}

//Ham cux
//function getNationTotalAmt() {
//	//alert("Tinh tong so tien thuc nop");
//	var listSize = document.getElementById("listSize").value;
//	var amount = 0;
//	var totalAmount = 0;
//	var name = "";
//	var name2 = "";
//	var orgAmt = "";
//	var orgAmtVal = 0;
//	var amt = 0;
//	for (var i = 0; i < listSize; i++) {
//		name = "bingBong[" + i + "].taxAmt";
//		name2 = "bingBong[" + i + "].amt";
//		orgAmt = "orgTaxAmt["+ i + "]";
//
//
//		amount = stringToDouble(Trim(document.getElementById(name).value));
//
//		document.getElementById(name2).value = amount;
//		totalAmount += amount;
//
//		document.getElementById(orgAmt).innerHTML = formatValueCurrency2(amount);
//
//	}
//	document.getElementById("totalAmt").value = totalAmount;
//	document.getElementById("strRealAmt").value = formatValueCurrency2(totalAmount);
//
//	document.getElementById("totalOrgTaxAmt").innerHTML = formatValueCurrency2(totalAmount);
//
//	readNumber2Text(totalAmount);
//	return true;
//}

function checkDefault(object) {
	var objVal = object.value;
	if (objVal == "" || objVal.length == 0) {
		object.value = 0;
	}
}
/**
 *
 * @param object
 * @return
 */
function formatAmt(object) {
	num = object.value.toString().replace(/\$|\,/g,'');
	if (object.value == '') {
		num = "0";
	}
	num = $.trim(num);
	if (num.length == 0){
		return false;
	}
	if(isNaN(num)) {
		return false;
	}
	var index = num.indexOf('.');
	var phannguyen;
	var thapphan;
	if(index < 0) {
		//Ko co phan thap phan
		thapphan = "00";
		phannguyen = num;
	} else {
		phannguyen = num.substring(0, index);
		thapphan = num.substring(index+1, num.length);

		//Xu ly phan thap phan
		if (thapphan.length < 2) {
			thapphan = thapphan + "0";
		} else if (thapphan.length > 2) {
			thapphan = thapphan.substring(0, 2);
		}
	}
	//Xu ly phan nguyen
	for (var i = 0; i < Math.floor((phannguyen.length-(1+i))/3); i++)
		phannguyen = phannguyen.substring(0,phannguyen.length-(4*i+3))+','+ phannguyen.substring(phannguyen.length-(4*i+3));
	object.value = phannguyen + "." + thapphan;

	return true;
}

//Validate currency fields
function formatValueCurrency2(objValue) {
		num = objValue.toString().replace(/\$|\,/g,'');
		if (Trim(num).length == 0){
			return false;
		}
		if(isNaN(num)) {
			alert(objValue + " la so tien nhap khong dung !");
			return false;
		}
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = Math.floor(num/100).toString();
		if(cents<10)
			cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+','+
			num.substring(num.length-(4*i+3));
		objValue = (num + '.' + cents);

		//Kiem tra phan le sau thap phan
		if (stringToDouble(cents) > 0) {
			alert("So tien khong hop le. Khong chap nhan phan le sau thap phan!");
			return false;
		}
		return objValue;
}
function formatCurrency4(object) {
	num = object.value.toString().replace(/\$|\,/g,'');
	if (object.value == '') {
		num = 0;
	}
	if (Trim(num).length == 0){
		return false;
	}
	if(isNaN(num)) {
		alert(object.value + " la so tien nhap khong dung !");
		return false;
	}
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
	object.value = (num + '.' + cents);

	//Kiem tra phan le sau thap phan
	if (stringToDouble(cents) > 0) {
		alert("So tien khong hop le. Khong chap nhan phan le sau thap phan!");
		return false;
	}

	//Tinh tong so tien thuc nop
	getNationTotalAmt();

	return true;
}


function RTrim(String)
{
	var i = 0;
	var j = String.length - 1;

	if (String == null)
		return (false);

	for(j = String.length - 1; j >= 0; j--)
	{
		if (String.substr(j, 1) != ' ' &&
			String.substr(j, 1) != '\t')
		break;
	}

	if (i <= j)
		return (String.substr(i, (j+1)-i));
	else
		return ('');
}
function LTrim(String)
{
	var i = 0;
	var j = String.length - 1;

	if (String == null)
		return (false);

	for (i = 0; i < String.length; i++)
	{
		if (String.substr(i, 1) != ' ' &&
			String.substr(i, 1) != '\t')
			break;
	}

	if (i <= j)
		return (String.substr(i, (j+1)-i));
	else
		return ('');
}

function Trim(String)
{
	if (String == null)
		return (false);

	return RTrim(LTrim(String));
}

//Format value to currency
function formatValueCurrency(objValue) {
	num = objValue.toString().replace(/\$|\,/g,'');
	if (Trim(num).length == 0){
		return false;
	}
//	if(isNaN(num)) {
//		alert(objValue + " la so tien nhap khong dung !");
//		return false;
//	}
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
		num.substring(num.length-(4*i+3));
	objValue = (num + '.' + cents);

	//Kiem tra phan le sau thap phan
//	if (stringToDouble(cents) > 0) {
//		alert("So tien khong hop le. Khong chap nhan phan le sau thap phan!");
//		return false;
//	}
	return objValue;
}

function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31;
        if (i==4 || i==6 || i==9 || i==11) {
            this[i] = 30;
        }
        if (i==2) {
            this[i] = 29;
        }
   }
   return this;
}

var dtCh= "/";
var dtCh2 = ":";
var minYear=1900;
var maxYear=9999;

function isDate(object){
	var dtStr = object.value;
	if(dtStr.length <= 0) {
		//alert("Ban chua nhap du thong tin!");
		return false;
	}
	var daysInMonth = DaysArray(12);
	var pos1 = dtStr.indexOf(dtCh);
	var pos2 = dtStr.indexOf(dtCh, pos1 + 1);
	var strDay = dtStr.substring(0, pos1);
	var strMonth = dtStr.substring(pos1 + 1, pos2);
	var strYear = dtStr.substring(pos2 + 1, pos2 + 5);

	strYr = strYear;
	if (strDay.charAt(0) == "0" && strDay.length > 1){
		strDay = strDay.substring(1);
	}
	if (strMonth.charAt(0)=="0" && strMonth.length > 1){
		strMonth=strMonth.substring(1);
	}
	for (var i = 1; i <= 5; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1){
			strYr=strYr.substring(1);
		}
	}

	month = parseInt(strMonth);
	day = parseInt(strDay);
	year = parseInt(strYr);

	if (pos1 == -1 || pos2 == -1) {
		//alert("Ngay chua dung dinh dang : dd/mm/yyyy");
		return false;
	}
	if (isNaN(strMonth) || strMonth.length<1 || month<1 || month>12) {
		//alert("Nhap thang sai!");
		return false;
	}
	if (isNaN(strDay) || strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
			//alert("Nhap ngay sai!");
		return false;
	}
	if (isNaN(strYr) || strYear.length != 4 || year==0) {
		//alert("Nhap nam sai!");
		return false;
	}

	return true;
}


/**
 * Loai bo dau trong noi dung
 * @param content
 * @return
 */
function removeAccents(object) {
  	var str1 = object.value;

  	str1 = str1.replace(/\u00e0/g, 'a');
  	str1 = str1.replace(/\u00e1/g, 'a');
  	str1 = str1.replace(/\u1ea3/g, 'a');
  	str1 = str1.replace(/\u00e3/g, 'a');
  	str1 = str1.replace(/\u1ea1/g, 'a');

  	str1 = str1.replace(/\u0103/g, 'a');
  	str1 = str1.replace(/\u1eb1/g, 'a');
  	str1 = str1.replace(/\u1eaf/g, 'a');
  	str1 = str1.replace(/\u1eb3/g, 'a');
  	str1 = str1.replace(/\u1eb5/g, 'a');
  	str1 = str1.replace(/\u1eb7/g, 'a');

  	str1 = str1.replace(/\u00e2/g, 'a');
  	str1 = str1.replace(/\u1ea7/g, 'a');
  	str1 = str1.replace(/\u1ea5/g, 'a');
  	str1 = str1.replace(/\u1ea9/g, 'a');
  	str1 = str1.replace(/\u1eab/g, 'a');
  	str1 = str1.replace(/\u1ead/g, 'a');

  	str1 = str1.replace(/\u00c0/g, 'A');
  	str1 = str1.replace(/\u00c1/g, 'A');
  	str1 = str1.replace(/\u1ea2/g, 'A');
  	str1 = str1.replace(/\u00c3/g, 'A');
  	str1 = str1.replace(/\u1ea0/g, 'A');

  	str1 = str1.replace(/\u0102/g, 'A');
  	str1 = str1.replace(/\u1eb0/g, 'A');
  	str1 = str1.replace(/\u1eae/g, 'A');
  	str1 = str1.replace(/\u1eb2/g, 'A');
  	str1 = str1.replace(/\u1eb4/g, 'A');
  	str1 = str1.replace(/\u1eb6/g, 'A');

  	str1 = str1.replace(/\u00c2/g, 'A');
  	str1 = str1.replace(/\u1ea6/g, 'A');
  	str1 = str1.replace(/\u1ea4/g, 'A');
  	str1 = str1.replace(/\u1ea8/g, 'A');
  	str1 = str1.replace(/\u1eaa/g, 'A');
  	str1 = str1.replace(/\u1eac/g, 'A');

  	str1 = str1.replace(/\u0111/g, 'd');
  	str1 = str1.replace(/\u0110/g, 'D');

  	str1 = str1.replace(/\u00e8/g, 'e');
  	str1 = str1.replace(/\u00e9/g, 'e');
  	str1 = str1.replace(/\u1ebb/g, 'e');
  	str1 = str1.replace(/\u1ebd/g, 'e');
  	str1 = str1.replace(/\u1eb9/g, 'e');

  	str1 = str1.replace(/\u00ea/g, 'e');
  	str1 = str1.replace(/\u1ec1/g, 'e');
  	str1 = str1.replace(/\u1ebf/g, 'e');
  	str1 = str1.replace(/\u1ec3/g, 'e');
  	str1 = str1.replace(/\u1ec5/g, 'e');
  	str1 = str1.replace(/\u1ec7/g, 'e');

  	str1 = str1.replace(/\u00c8/g, 'E');
  	str1 = str1.replace(/\u00c9/g, 'E');
  	str1 = str1.replace(/\u1eba/g, 'E');
  	str1 = str1.replace(/\u1ebc/g, 'E');
  	str1 = str1.replace(/\u1eb8/g, 'E');

  	str1 = str1.replace(/\u00ca/g, 'E');
  	str1 = str1.replace(/\u1ec0/g, 'E');
  	str1 = str1.replace(/\u1ebe/g, 'E');
  	str1 = str1.replace(/\u1ec2/g, 'E');
  	str1 = str1.replace(/\u1ec4/g, 'E');
  	str1 = str1.replace(/\u1ec6/g, 'E');

  	str1 = str1.replace(/\u00ec/g, 'i');
  	str1 = str1.replace(/\u00ed/g, 'i');
  	str1 = str1.replace(/\u1ec9/g, 'i');
  	str1 = str1.replace(/\u0129/g, 'i');
  	str1 = str1.replace(/\u1ecb/g, 'i');

  	str1 = str1.replace(/\u00cc/g, 'I');
  	str1 = str1.replace(/\u00cd/g, 'I');
  	str1 = str1.replace(/\u1ec8/g, 'I');
  	str1 = str1.replace(/\u0128/g, 'I');
  	str1 = str1.replace(/\u1eca/g, 'I');

  	str1 = str1.replace(/\u00f2/g, 'o');
  	str1 = str1.replace(/\u00f3/g, 'o');
  	str1 = str1.replace(/\u1ecf/g, 'o');
  	str1 = str1.replace(/\u00f5/g, 'o');
  	str1 = str1.replace(/\u1ecd/g, 'o');

  	str1 = str1.replace(/\u00f4/g, 'o');
  	str1 = str1.replace(/\u1ed3/g, 'o');
  	str1 = str1.replace(/\u1ed1/g, 'o');
  	str1 = str1.replace(/\u1ed5/g, 'o');
  	str1 = str1.replace(/\u1ed7/g, 'o');
  	str1 = str1.replace(/\u1ed9/g, 'o');

  	str1 = str1.replace(/\u01a1/g, 'o');
  	str1 = str1.replace(/\u1edd/g, 'o');
  	str1 = str1.replace(/\u1edb/g, 'o');
  	str1 = str1.replace(/\u1edf/g, 'o');
  	str1 = str1.replace(/\u1ee1/g, 'o');
  	str1 = str1.replace(/\u1ee3/g, 'o');

  	str1 = str1.replace(/\u00d2/g, 'O');
  	str1 = str1.replace(/\u00d3/g, 'O');
  	str1 = str1.replace(/\u1ece/g, 'O');
  	str1 = str1.replace(/\u00d5/g, 'O');
  	str1 = str1.replace(/\u1ec6/g, 'O');

  	str1 = str1.replace(/\u00d4/g, 'O');
  	str1 = str1.replace(/\u1ed2/g, 'O');
  	str1 = str1.replace(/\u1ed0/g, 'O');
  	str1 = str1.replace(/\u1ed4/g, 'O');
  	str1 = str1.replace(/\u1ed6/g, 'O');
  	str1 = str1.replace(/\u1ed8/g, 'O');

  	str1 = str1.replace(/\u01a0/g, 'O');
  	str1 = str1.replace(/\u1edc/g, 'O');
  	str1 = str1.replace(/\u1eda/g, 'O');
  	str1 = str1.replace(/\u1ede/g, 'O');
  	str1 = str1.replace(/\u1ee0/g, 'O');
  	str1 = str1.replace(/\u1ee2/g, 'O');

  	str1 = str1.replace(/\u00f9/g, 'u');
  	str1 = str1.replace(/\u00fa/g, 'u');
  	str1 = str1.replace(/\u1ee7/g, 'u');
  	str1 = str1.replace(/\u0169/g, 'u');
  	str1 = str1.replace(/\u1ee5/g, 'u');

  	str1 = str1.replace(/\u01b0/g, 'u');
  	str1 = str1.replace(/\u1eeb/g, 'u');
  	str1 = str1.replace(/\u1ee9/g, 'u');
  	str1 = str1.replace(/\u1eed/g, 'u');
  	str1 = str1.replace(/\u1eef/g, 'u');
  	str1 = str1.replace(/\u1ef1/g, 'u');

  	str1 = str1.replace(/\u00d9/g, 'U');
  	str1 = str1.replace(/\u00da/g, 'U');
  	str1 = str1.replace(/\u1ee6/g, 'U');
  	str1 = str1.replace(/\u0168/g, 'U');
  	str1 = str1.replace(/\u1ee4/g, 'U');

  	str1 = str1.replace(/\u01af/g, 'U');
  	str1 = str1.replace(/\u1eea/g, 'U');
  	str1 = str1.replace(/\u1ee8/g, 'U');
  	str1 = str1.replace(/\u1eec/g, 'U');
  	str1 = str1.replace(/\u1eee/g, 'U');
  	str1 = str1.replace(/\u1ef0/g, 'U');

  	str1 = str1.replace(/\u00ec/g, 'i');
  	str1 = str1.replace(/\u00ed/g, 'i');
  	str1 = str1.replace(/\u1ec9/g, 'i');
  	str1 = str1.replace(/\u0129/g, 'i');
  	str1 = str1.replace(/\u1ecb/g, 'i');

  	str1 = str1.replace(/\u00cc/g, 'I');
  	str1 = str1.replace(/\u00cd/g, 'I');
  	str1 = str1.replace(/\u1ec8/g, 'I');
  	str1 = str1.replace(/\u0128/g, 'I');
  	str1 = str1.replace(/\u1eca/g, 'I');

  	str1 = str1.replace(/\u1ef3/g, 'y');
  	str1 = str1.replace(/\u00fd/g, 'y');
  	str1 = str1.replace(/\u1ef7/g, 'y');
  	str1 = str1.replace(/\u1ef9/g, 'y');
  	str1 = str1.replace(/\u1ef5/g, 'y');

  	str1 = str1.replace(/\u1ef2/g, 'Y');
  	str1 = str1.replace(/\u00dd/g, 'Y');
  	str1 = str1.replace(/\u1ef6/g, 'Y');
  	str1 = str1.replace(/\u1ef8/g, 'Y');
  	str1 = str1.replace(/\u1ef4/g, 'Y');


	str1= str1.replace("--","-"); //thay thế 2- thành 1-
	object.value = str1;
	return;
}
