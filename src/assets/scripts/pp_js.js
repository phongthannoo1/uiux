var thisForm = document.contactFrm;
    //var parentForm = window.opener.document;
    var parentForm = window.parent.document;
    function doClose() {
    	window.close();
    	return;
    }
    function Quit(){   
    	window.close();
    }
    function doAccept() {
    	var objName = "";
    	var objId = "";
    	var index = 0;
    	var len = thisForm.elements.length;
    	var bnkCde = "";
    	var bnkName = "";
    	var bnkaddr = "";
    	var recvAddr = "";
    	var recvacc = "";
    	var recvcode = "";
		//Lay gia tri recvAcct
		for (i=0; i<len; i++) {
			var e = thisForm.elements[i];
			if (e.name == "recvAcct" && e.checked == true) {
				index++;
				objId = e.value;
			}
		}
		if (index == 0) {
			alert("Quy khach chua chon tai khoan nguoi thu huong!");
			return;
		}
        //Lay gia tri content
        for (j=0; j<len; j++) {
        	var f = thisForm.elements[j];
        	if (f.name == "recvacc["+objId+"]") {
        		recvacc = f.value;
        	} else if (f.name == "recvname["+objId+"]") {
        		objName = f.value;
        	} else if (f.name == "bnkcde["+objId+"]") {
        		bnkCde = f.value;
        	} else if (f.name == "recvaddr["+objId+"]") {
        		recvAddr = f.value;
        	} else if (f.name == "bnkname["+objId+"]") {
        		bnkName = f.value;
        	} else if (f.name == "bnkaddr["+objId+"]") {
        		bnkaddr = f.value;
        	} else if (f.name == "recvcode["+objId+"]") {
        		recvcode = f.value;
        	}  
        }

        var frId = document.getElementById("fromId").value;
		//if (window.opener && !window.opener.closed) {
			if (frId == "1") {
				if(parentForm.paymentForm != null) {
			    	//parentForm.getElementById("creditac").value = recvacc; 
			    	parentForm.getElementById("creditac").value = recvacc;
			    } 
			} else if (frId == "2") {
				if(parentForm.outpaymentForm != null) {
					parentForm.getElementById("creditac").value = recvacc; 
					parentForm.getElementById("payee").value = objName; 
					parentForm.getElementById("outbank").value = bnkName;
				} 
			} else if (frId == "3") {
				parentForm.getElementById("recvcustacc").value = recvacc; 
				parentForm.getElementById("recvcustname").value = objName + " " + recvAddr;
			} else if (frId == "4") {
				if(parentForm.outpaymentForm != null) {
					parentForm.getElementById("creditac").value = recvacc; 
					parentForm.getElementById("payee").value = objName; 
					$(parentForm).find('#branchCode').append("<option value='" + recvcode + "' selected>" + recvcode + " - " + bnkName + "</option>");
				} 
			}
		// }
		window.close();

	}

	(function($) {
		$.fn.stacktable = function(options) {
			var $tables = this,
			defaults = {
				id: 'stacktable',
				hideOriginal: false
			},
			settings = $.extend({}, defaults, options);
			return $tables.each(function() {
				var $stacktable = $('<table class="' + settings.id + '"><thead></thead><tbody></tbody></table>');
				if (typeof settings.myClass !== undefined) $stacktable.addClass(settings.myClass);
				var markup = '';
				$table = $(this);
				$topRow = $table.find('tr').eq(0);
				
				$table.find('tr').each(function(index, value) {
					trueIndex = index - 1;
					markup += '<tr onclick="javascript:doAccept(' + trueIndex + ');" style="cursor: pointer;"><td>'; {
						$(this).find('td').each(function(index, value) {
							{
								if ($(this).html() !== '') {
									markup += '<div class="clearfix" >';
									if ($topRow.find('td,th').eq(index).html()) {
										markup += '<div class="rps-table-key col-rps-6">' + $topRow.find('td,th').eq(index).html() + ':&nbsp;</div>';
									} else {
										markup += '<div class="rps-table-key col-rps-6"></div>';
									}
									markup += '<div class="rps-table-val col-rps-6">' + $(this).html() +  '</div>';
									markup += '</div>';
								}
							}
						});
					}
					markup += '</td></tr>';
				});
				$stacktable.append($(markup));
				$table.before($stacktable);
				if (settings.hideOriginal) $table.hide();
			});
};


}(jQuery));

$(function() {
$('table.popup_data').stacktable({
	myClass: 'respondsive-added'
}); 
});