// Tks VCB
function formatNumberVND(e) {
    e.parseNumber({ format: "#,##0", locale: "us" });
    e.formatNumber({ format: "#,##0", locale: "us" });
}
function formatNumberUSD(e) {
    if (e.val().match(/^.+?\.0?$/)) return;
    e.parseNumber({ format: "#,##0.##", locale: "us" });
    e.formatNumber({ format: "#,##0.##", locale: "us" });
}
function formatNumberUSD00(e) {
    if (e.val().match(/^.+?\.0?$/)) return;
    e.parseNumber({ format: "#,##0.00", locale: "us" });
    e.formatNumber({ format: "#,##0.00", locale: "us" });
}
function formatNumberRate(e) {
	if (e.val().match(/^.+?\.0?$/)) return;
    e.parseNumber({ format: "#,##0.#####", locale: "us" });
    e.formatNumber({ format: "#,##0.#####", locale: "us" });
}

function formatExchangeRate(e) {	
    e.parseNumber({ format: "#,##0.#####", locale: "us" });
    e.formatNumber({ format: "#,##0.#####", locale: "us" });
}
var ctrlDown = false;
function handleKeyDown(e) {
    if (e.which == 17) ctrlDown = true;
}
function handleKeyUp(e) {
    if (e.which == 17) ctrlDown = false;
}
function ignoreEvent(e) {
    if (e.which >= 16 && e.which <= 18) return true;
    if (e.which >= 33 && e.which <= 40) return true;
    if (ctrlDown && (e.which == 65 || e.which == 67)) return true;
    return false;
}

// Execute when page is load
$(document).ready(function() {
    $(".amountVND").each(function() {
        formatNumberVND($(this));
    });

    $(".amountUSD").each(function() {
        formatNumberUSD($(this));
    });

    $(".amountUSD00").each(function() {
        formatNumberUSD00($(this));
    });
    
    $(".amountRate").each(function() {
    	formatNumberRate($(this));
    });
    $(".fomartRate").each(function() {
    	formatExchangeRate($(this));
    });
    $(".amountVND").keydown(function(e) {
        handleKeyDown(e);
    }).keyup(function(e) {
        handleKeyUp(e);
        if (!ignoreEvent(e)) formatNumberVND($(this));
    });

    $(".amountUSD").keydown(function(e) {
        handleKeyDown(e);
    }).keyup(function(e) {
        handleKeyUp(e);
        if (!ignoreEvent(e)) formatNumberUSD($(this));
    });

    $(".panel-heading").click(function() {
        var aaa = $(this).next();
        if ($(aaa).is(':visible')) {
            $(aaa).slideUp('fast');
        } else {
            $(aaa).slideDown('fast');
        }
    });

    if ($('body').hasClass('login-page')) {
        var loginheight = window.innerHeight;
        // var loginheight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        elmheader = document.getElementById('header');
        elmfooter = document.getElementById('footer');
        var headerH = elmheader.clientHeight;
        var footerH = elmfooter.clientHeight;
        otherHeight = headerH + footerH + 30;
        $('#main').css('min-height', loginheight - otherHeight);
        /* Login page slider */
        $('#login_slider').flexslider({
            animation: "slide",
            controlNav: true,
            pauseOnAction: true
        });
    } else {
        onResize = (function() {
            // Min-height main-right, and width window
            var element = document.getElementById('main_navi');
            var live_height = element.clientHeight;
            var winHeight = window.innerHeight;
            elmheader = document.getElementById('header');
            elmfooter = document.getElementById('footer');
            var headerH = elmheader.clientHeight;
            var footerH = elmfooter.clientHeight;
            otherHeight = headerH + footerH + 30;
            $('#main').css('min-height', Math.max(winHeight - otherHeight, live_height));
         });

        $('#main').wrapAll("<div id='outer_main'></div>");
        $('#header').each(
            function() {
                $(this).find('.logo-box.container.clearfix,.nav-box, .user-box.container').wrapAll('<div class="summary-inside"></div>');
            });
        $("body").append('<div id="overlay_menu"></div>');
        onResize();

        /* SHOW Navigation */
        $('#sub-nav').click(function() {
            $inner = $('#main, .summary-inside');
            $inner.stop().animate({
                left: '243px'
            }, 500);
            $('#overlay_menu').css('min-height', $(document).height());
            $('#overlay_menu').css({
                'display' : 'block'
            })
            $('#outer_main').addClass('menu_open');
        });
        /* HIDE Navigation */
        $('#overlay_menu').click(function() {
            $inner = $('#main, .summary-inside');
            $inner.stop().animate({
                left: '0'
            }, 500);
            $('#outer_main').removeAttr('menu_open');
            $('#overlay_menu').removeAttr('style');
        });
    }

    // $('.main-right').wrapInner("<div class='new'></div>");

    /* Datepicker */
    $('.datepicker').pikaday({
        format: 'DD/MM/YYYY'
    });

    /* Top-nav sub menu (Notifications) */
    $(document).click(function() {
        $('.nav-box .noti').find('ul').hide();
        $('.nav-box .noti').removeClass('showing');
        $('#header').removeAttr('style');
    });

    $('.nav-box .noti').find('a').click(function() {

        if ($(this).siblings().size() > 0) {
            $('#header').css('overflow', 'visible');
            $(this).siblings().toggle();
            $(this).parent().toggleClass('showing');
            return false;
        }
    });

    /* Accordion Nanigation */
    $('.main-navigation > li > span').click(function() {
        var thisItem = $(this).parent();
        $(thisItem).children('ul').stop().slideToggle(function() {
            onResize();
        });
        $('.main-navigation > li').not(thisItem).children('ul').stop().slideUp();
    });

    /* SHOW-HIDE Table */
    $('.table-title.toggle').click(function() {
        var thisTitle = $(this);
        $(thisTitle).next().slideToggle(function() {
            $(thisTitle).toggleClass('hiding');
        });
    });

    /* Footer column menu */
    $('#footer > #top').find('h3').click(function() {
        var thisH3 = $(this);
        var windowWidth = $(window).width();
        if (windowWidth <= 700) {
            $(thisH3).next().slideToggle();
        }
    });

    /* Account detail addition-info */
    $('.addition-info').hide().css("overflow", "hidden");
    $('.more_button').click(function(e) {
        var link = $(this);
        $('.addition-info').slideToggle('fast', function() {
            if ($(this).is(":visible")) {
                $('.more_button').addClass('less_button');
            } else {
                $('.more_button').removeClass('less_button');
            }
        });
        return false;
    });

    /* Table */
    $('.page-table table').wrap('<div class="table-wrap"></div>');
    $('.page-table td:empty').addClass('empty');
    $('.page-table table').each(function() {
        $(this).find('th').each(function(index) {
            var indexNew = index + 1,
                tableNew = $(this).parents('table');
            $(tableNew).find('td:nth-child(' + indexNew + ')').wrapInner('<span class="content">').prepend('<div class="td_title"><span class="title">' + $(this).text() + ':</span></div>');
        });
    });

    /* Alert dismissable */
    $('.alert-dismissable').find('button.close').click(function() {
        $(this).parent().fadeOut();
    });


    $(":radio").uniform();
    $(":checkbox").uniform();

    /* Checkbox-selected */
    $('.checkbox-selected').on('click', function() {
        var anableField = $(this).val();
        var targetField = $(this).attr('for');

        $(anableField).prop('disabled', false).first().focus();
        $('.' + targetField).not(anableField).prop('disabled', true).val('');

        $.uniform.update();
    });


    /* Check/uncheck all */
    $('.checkbox-all').change(function() {
        var targetName = $(this).attr('for');
        var checkValue = $(this).prop('checked');

        $('input:checkbox.' + targetName).prop('checked', checkValue);
        $.uniform.update();
    });

    /* Check/uncheck item */
    $('input:checkbox').change(function() {
        var thisName = $(this).attr('class');
        var checkboxAll = $('input.checkbox-all[for=' + thisName + ']');

        if (checkboxAll.length > 0) {
            var targetName = $(checkboxAll).attr('for');
            targetBoxes = $('input:checkbox.' + targetName);

            if ($(targetBoxes).length == $(targetBoxes).prop('checked').length) {
                $(checkboxAll).prop('checked', true);
            } else {
                $(checkboxAll).prop('checked', false);
            }

            $.uniform.update();
        }
    });

    /* Popup fancyBox */
    $(".fancybox").fancybox({
        width: '100%',
        height: '100%',
        maxWidth: 800,
        maxHeight: 600
        //fitToView	: false,
        // autoSize	: false,
        // closeClick	: false,
        // openEffect	: 'none',
        // closeEffect	: 'none'
    });

    /* Multi tab */
    $("ul.multitab").tinyNav();

    /* accordion toggle */
    $(".accordion-toggle").click(function() {
        var elementID = $(this).attr('href');
        var thisTitle = $(this);
        if ($(elementID).is(':visible')) {
            $(elementID).slideUp('fast');
            $(thisTitle).addClass('collapsed');
        } else {
            $(elementID).slideDown('fast');
            $(thisTitle).removeClass('collapsed');
        }
    });

	/* Add Inactive Class To All Accordion Headers */
    $('.accordion-header').toggleClass('inactive-header');

    /* Set The Accordion Content Width */
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});

    /* Open The First Accordion Section When Page Loads */
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');

    /* The Accordion Effect */
    $('.accordion-header').click(function() {
        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        } else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });

    $('.sub-accordion-header').toggleClass('inactive-header');
    $('.sub-accordion-header').click(function() {
        if ($(this).is('.inactive-header')) {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
        } else {
            $(this).toggleClass('inactive-header').toggleClass('active-header');
        }
        $(this).next().slideToggle().toggleClass('open-content');
    });

    // Tooltip question mark
    $("i.question").hover(function () {
        var tooltipText = $(this).attr('title');
        $(this).append('<div class="tooltip"><p>' + tooltipText + '</p></div>');
    }, function () {
        $("div.tooltip").remove();
    });

     /* Back to top action */
    var offset = 230;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 79
        }, duration);
        return false;
    });

    /* Scrolleable table */
    var rowCount = $('.list-trans-ctk table tr').length;
    var rowCount1 = $('.radio-list table tr').length;
    if ((rowCount >= 30) || (rowCount1 >= 30)) {
        $('.table-wrap').css({
            'position': 'relative',
            'max-height': '700px',
            'overflow-y': 'scroll'
        });
    };
});


/*SCF :: Begin*/
var title;
var changeTooltipPosition = function(event) {
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var tooltipWidth = $('div.scf-tooltip').width();
	var tooltipHeight = $('div.scf-tooltip').height();

	var tooltipX,tooltipY;
	if(event.clientX + tooltipWidth - 8 > windowWidth){
		tooltipX = event.pageX - tooltipWidth - 10;
	}else{
		tooltipX = event.pageX;
	}
	
	if(event.clientY + tooltipHeight > windowHeight){
		tooltipY = event.pageY - tooltipHeight - 10;
	}else{
		tooltipY = event.pageY + 22;
	}

	$('div.scf-tooltip').css({
		top : tooltipY,
		left : tooltipX
	});
};
var showTooltip = function(event) {				
	$('div.tooltip').remove();
	title = $(this).closest('.hint').attr("title");
	$('<div class="scf-tooltip">'
		+ '<h3 class="scf-tooltip-title">Infomation</h3>'
		+ '<div class="scf-tooltip-content">' + title + '</div>'
	+ '</div>').appendTo('body');					
	$(this).removeAttr('title');					
	changeTooltipPosition(event);
};
var hideTooltip = function() {
	$(this).attr("title", title);
	$('div.scf-tooltip').remove();
};
/*SCF :: End*/