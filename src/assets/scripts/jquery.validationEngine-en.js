(function($) {
	$.fn.validationEngineLanguage = function() {
	};
	$.validationEngineLanguage = {
		newLang : function() {
			$.validationEngineLanguage.allRules = {
				"required" : { // Add your regex rules here, you can take
								// telephone as an example
					"regex" : "none",
					"alertText" : "This field cannot be blank. Please input valid value.",
					"alertTextCheckboxMultiple" : "Must select an option",
					"alertTextCheckboxe" : "Please accept the term and condition"
				},
				"length" : {
					"regex" : "none",
					"alertText" : "Cho ph&#233;p t&#7915; ",
					"alertText2" : " t&#7899;i ",
					"alertText3" : " k&#253; t&#7921;"
				},
				"maxCheckbox" : {
					"regex" : "none",
					"alertText" : "Checks allowed Exceeded"
				},
				"minCheckbox" : {
					"regex" : "none",
					"alertText" : "Ch&#7885;n t&#7889;i thi&#7875;u ",
					"alertText2" : " th&#244;ng tin"
				},
				"confirm" : {
					"regex" : "none",
					"alertText" : "Your field is not matching"
				},
				"telephone" : {
					"regex" : /^[0-9\-\(\)\ ]+$/,
					"alertText" : "Invalid phone number"
				},
				"email" : {
					"regex" : /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/,
					"alertText" : "Invalid email address"
				},
				"date0" : {
					"regex" : /^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
					"alertText" : "Invalid date, must be in YYYY-MM-DD format"
				},
				"date" : {
					"regex" : /^[0-3][0-9][\/](([0][0-9])|([1][0-2]))[\/][0-9]{4}$/,
					"alertText" : "Please input date with format dd/mm/yyyy"
				},
				"onlyNumber" : {
					"regex" : /^[0-9\ ]+$/,
					"alertText" : "Please input number only"
				},
				"paycode" : {
					"regex" : /^[0-9\-]+$/,
					"alertText" : "Invalid paycode number"
				},
				"noSpecialCaracters" : {
					"regex" : /^[^@!~#&%$*{}^\"]+$/,
					"alertText" : "Input text cannot contain the following letters: @, !, ~, #, &, %, $, *, {, }, ^\""
				},
				"ajaxUser" : {
					"file" : "trxTransfer.jsp",
					"extraData" : "name=eric",
					"alertTextOk" : "This user is available",
					"alertTextLoad" : "Loading, please wait",
					"alertText" : "This user is already taken"
				},
				"ajaxName" : {
					"file" : "validateUser.php",
					"alertText" : "This name is already taken",
					"alertTextOk" : "This name is available",
					"alertTextLoad" : "Loading, please wait"
				},
				"onlyLetter" : {
					"regex" : /^[a-zA-Z\ \']+$/,
					"alertText" : "Input alphabet character only!"
				},
				"validate2fields" : {
					"nname" : "validate2fields",
					"alertText" : "You must have a firstname and a lastname"
				},
				"onlyNam" : {
					"regex" : /^[0-9]{1,3}(\,[0-9]{3})*\.[0]{2}$/,
					"alertText" : "Numeric character only!"
				},
				"onlyCurrency" : {
					"regex" : /^[0-9]{1,3}([,][0-9]{3})*\.[0-9]{2}$/,
					"alertText" : "Numeric character only!"
				},
				"onlyCurrencyInteger" : {
					"regex" : /^[0-9]{1,3}([,][0-9]{3})*\.[0]{2}$/,
					"alertText" : "Integer number only!"
				},
				"checktaxno" : {
					"regex" : /^([0-9]{10}\-[0-9]{3})|([0-9]{10})$/,
					"alertText" : "Invalid tax code format"
				},
				"onlyNamVND" : {
					"regex" : /^[0-9]{1,3}(\,[0-9]{3})*((\.[0-9]{2})|(\.[1-9])|())$/,
					"alertText" : "Must be in digit"
				},
				"onlyNamDATE" : {
					"regex" : /^[0-3][1-9][\/][0-1][1-9][\/][2][0-9]{3}$/,
					"alertText" : "Invalid date format"
				},
				 "checkDate6" : {
						"nname" : "checkDate6",
						"alertText" : "Must be in mm/yyyy format"
					},
				"checkDate8" : {
					"nname" : "checkDate8",
					"alertText" : "Must be in dd/mm/yy format"
				},
				"checkDate7" : {
					"nname" : "checkDate7",
					"alertText" : "Must be in mm/yyyy format"
				},
				"checkFrDate" : {
					"nname" : "checkFrDate",
					"alertText" : "Must be in dd/mm/yyyy format"
				},
				"checkToDate" : {
					"nname" : "checkToDate",
					"alertText" : "Must be required and in dd/mm/yyyy format"
				},
				"checkDate1" : {
					"nname" : "checkDate1",
					"alertText" : "Must be in dd/mm/yyyy format"
				},
				"checkDate2" : {
					"nname" : "checkDate2",
					"alertText" : "Must be in dd/mm/yyyy format"
				},
				"checkFX" : {
					"nname" : "checkFX",
					"alertText" : "Exchange rate must  be in X,XXX.XXXXXXX and not greater than 99,999.9999999"
				},
				"checkNum" : {
					"nname" : "checkNum",
					"alertText" : "Net income must greater than 0"
				},
				
				//Customize
				"checkAccountNumber":{
                    "regex": /^[\da-zA-Z]+$/,
                    "alertText": "The account number must include  only characters and digits"
                },
                "checkPassVerify":{
                    "regex": /^[\da-zA-Z]+$/,
                    "alertText": "The password must include only characters and digits"
                },
                "checkAgencyCode":{
                    "regex": /^[\da-zA-Z]+$/,
                    "alertText": "Must include  only characters and digits"
                },
                
                "onlySheetNo":{
                    "regex": /^[\da-zA-Z]+$/,
                    "alertText": "The Original Tax Payment Table No must include  only characters and digits"
                },
                "checkPersonName":{
                    "regex": /^[ ]*[a-zA-Z]+[\da-zA-Z ]*$/,
                    "alertText": "Must be charaters withour marks, digits and space"
                },
                "onlyAlphaNumeric" : {
                    "regex" : /^[0-9a-zA-Z]+$/,
                    "alertText" : "Must be digits or characters a-z 0-9"
                },
                "onlyMonthYear":{
                    "regex" : /^(0[1-9]|1[012])\/[\d]{4}$/,
                    "alertText" : "Must be in mm/yyyy format"
                },
                "validContent":{
                    "regex": /^[\da-zA-Z,\@\#\$\!\%\^\&\*\-\_;.\/\(\)+\[\] ]+$/,
                    "alertText": "Must be characters, digits and special sign: & * % $ # @ () , - ; . / \ + [ ] : ^ "
                },
//              ThanhMT bo sung chan cho cac giao dich EFAST
                "validContent_gdEfast":{
                	 "regex": /^[\da-zA-Z,\@\#\$\!\%\^\&\*\-\_;.\/\(\)+\[\] ]+$/,
                     "alertText": "Must be characters, digits and special sign: & * % $ # @ () , - ; . / \ + [ ] : ^ "
                	 //"regex": /^[\da-zA-Z,\@\#\$\!\%\^\&\*\-\'\"\_;.\/\(\)+\[\] ]+$/,
                	 //"alertText": "Must be characters, digits and special sign: & * % $ # @ () , - ; . / \ + [ ] : ^ "
                                        	                                 					   
                },
                "validContentWithNewLine":{
                	"regex": /^[\da-zA-Z,\,\'\.\(\)\-\+\/\:\{\}\?\n ]+$/,
                    "alertText": "Must be characters, digits and special sign: , ' . () - + / : {} ? Limited to 35 characters per line, maximum 4 lines"
                },
                "normalDate":{
                    "regex" : /^(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/][\d]{4}$/,
                    "alertText" : "Must be in dd/mm/yyyy format"
                },
                "taxDate":{
                	"regex" : /^(0[0-9]|[12][0-9]|3[01])[\/]([[a-zA-z0-9][a-zA-z0-9]]?|00|0[1-9]|1[012])[\/][\d]{4}$/,
                    "alertText" : "Format is invalid"
                },
                "checkBalance":{
                	"alertText": "Debit amount must less available balance!"
                }
			};

		}
	};

	$.validationEngineLanguage.newLang();
})(jQuery);
