(function($) {
    $.fn.validationEngineLanguage = function() {
    };
    $.validationEngineLanguage = {
        newLang : function() {
            $.validationEngineLanguage.allRules = {
                "required" : {
                    "regex" : "none",
                    "alertText" : "Trường này không được trống",
                    "alertTextCheckboxMultiple" : "Bắt buộc chọn một phương thức",
                    "alertTextCheckboxe" : "Cam kết bắt buộc phải chọn"
                },
                "length" : {
                    "regex" : "none",
                    "alertText" : "Cho ph&#233;p t&#7915; ",
                    "alertText2" : " t&#7899;i ",
                    "alertText3" : " k&#253; t&#7921;"
                },
                "minCheckbox" : {
                    "regex" : "none",
                    "alertText" : "Ch&#7885;n t&#7889;i thi&#7875;u ",
                    "alertText2" : " th&#244;ng tin"
                },
                "date0" : {
                    "regex" : /^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/,
                    "alertText" : "Invalid date, must be in YYYY-MM-DD format"
                },
                "date" : {
                    "regex" : /^[0-3][0-9][\/](([0][0-9])|([1][0-2]))[\/][0-9]{4}$/,
                    "alertText" : "Nh&#7853;p ng&#224;y theo &#273;&#7883;nh d&#7841;ng dd/mm/yyyy"
                },
                "onlyNumber" : {
                    "regex" : /^[0-9\ ]+$/,
                    "alertText" : "Ch\u1ec9 cho ph\u00e9p ki\u1ec3u s\u1ed1"
                },
				"paycode" : {
					"regex" : /^[0-9\-]+$/,
					"alertText" : "M\u00E3 s\u1ED1 thu\u1EBF kh\u00F4ng h\u1EE3p l\u1EC7"
				},
                "noSpecialCaracters" : {
                    "regex" : /^[^@!~#&%$*{}^\"]+$/,
                    "alertText" : "Kh&#244;ng cho ph&#233;p c&#225;c k&#253; t&#7921; &#273;&#7863;c bi&#7879;t: @, !, ~, #, &, %, $, *, {, }, ^\""
                },
                "onlyLetter" : {
                    "regex" : /^[a-zA-Z\ \']+$/,
                    "alertText" : "Ch&#7881; nh&#7853;p ki&#7875;u ch&#7919;"
                },
                "validate2fields" : {
                    "nname" : "validate2fields",
                    "alertText" : "You must have a firstname and a lastname"
                },
                "onlyNam" : {
                    "regex" : /^[0-9]{1,3}(\,[0-9]{3})*\.[0]{2}$/,
                    "alertText" : "Ch\u1ec9 cho ph\u00e9p ki\u1ec3u s\u1ed1"
                },
                "onlyCurrency" : {
                    "regex" : /^[0-9]{1,3}([,][0-9]{3})*\.[0-9]{2}$/,
                    "alertText" : "Ch\u1ec9 cho ph\u00e9p ki\u1ec3u s\u1ed1"
                },
                "onlyCurrencyInteger" : {
                    "regex" : /^[0-9]{1,3}([,][0-9]{3})*\.[0]{2}$/,
                    "alertText" : "Ch\u1ec9 cho ph\u00e9p ki\u1ec3u s\u1ed1 nguy\u00EAn"
                },
                "checktaxno" : {
                    "regex" : /^([0-9]{10}\-[0-9]{3})|([0-9]{10})$/,
                    "alertText" : "Ma so thue khong dung dinh dang"
                },
                "onlyNamVND" : {
                    "regex" : /^[0-9]{1,3}(\,[0-9]{3})*((\.[0-9]{2})|(\.[1-9])|())$/,
                    "alertText" : "Ch\u1ec9 cho ph\u00e9p ki\u1ec3u s\u1ed1"
                },
                "onlyNamDATE" : {
                    "regex" : /^[0-3][1-9][\/][0-1][1-9][\/][2][0-9]{3}$/,
                    "alertText" : "Ng\u00e0y ch\u01b0a \u0111\u00fang \u0111\u1ecbnh d\u1ea1ng"
                },
                "checkDate6" : {
					"nname" : "checkDate6",
					"alertText" : "Nh&#7853;p ng&#224;y theo &#273;&#7883;nh d&#7841;ng mm/yyyy"
				},
                "checkDate8" : {
                    "nname" : "checkDate8",
                    "alertText" : "Nh&#7853;p ng&#224;y theo &#273;&#7883;nh d&#7841;ng dd/mm/yy"
                },
                "checkDate7" : {
                    "nname" : "checkDate7",
                    "alertText" : "Nh&#7853;p k&#7923; thu&#7871; theo &#273;&#250;ng &#273;&#7883;nh d&#7841;ng mm/yyyy"
                },
                "checkFrDate" : {
                    "nname" : "checkFrDate",
                    "alertText" : "Tr&#432;&#7901;ng ng&#224;y kh&#244;ng &#273;&#432;&#7907;c tr&#7889;ng, nh&#7853;p theo &#273;&#7883;nh d&#7841;ng dd/mm/yyyy"
                },
                "checkToDate" : {
                    "nname" : "checkToDate",
                    "alertText" : "Tr&#432;&#7901;ng ng&#224;y kh&#244;ng &#273;&#432;&#7907;c tr&#7889;ng, nh&#7853;p theo &#273;&#7883;nh d&#7841;ng dd/mm/yyyy"
                },
                "checkDate1" : {
                    "nname" : "checkDate1",
                    "alertText" : "Nh&#7853;p ng&#224;y theo &#273;&#7883;nh d&#7841;ng dd/mm/yyyy"
                },
                "checkDate2" : {
                    "nname" : "checkDate2",
                    "alertText" : "Nh&#7853;p ng&#224;y theo &#273;&#7883;nh d&#7841;ng dd/mm/yyyy"
                },
                "checkFX" : {
                    "nname" : "checkFX",
                    "alertText" : "T&#7927; gi&#225; theo &#273;&#7883;nh d&#7841;ng X,XXX.XXXXXXX v&#224; kh&#244;ng v&#432;&#7907;t qu&#225; 99,999.9999999"
                },
                "checkNum" : {
                    "nname" : "checkNum",
                    "alertText" : "Thu nh&#7853;p r&#242;ng ph&#7843;i l&#7899;n h&#417;n 0"
                },
                // HUY.VQ add more
                "checkAccountNumber":{
                    "regex": /^[\da-zA-Z]+$/,
                    "alertText": "Số tài khoản chỉ được phép nhập số và chữ cái"
                },
                "checkPassVerify":{
                    "regex": /^[\da-zA-Z]+$/,
                    "alertText": "Mật khẩu chỉ được phép nhập số và chữ cái"
                },
                "onlySheetNo":{
                    "regex": /^[\da-zA-Z]+$/,
                    "alertText": "Số bảng kê gốc chỉ được phép nhập số và chữ cái"
                },
                "checkPersonName":{
                    "regex": /^[ ]*[a-zA-Z]+[\da-zA-Z ]*$/,
                    "alertText": "Chỉ được phép nhập chữ cái không dấu, chữ số và khoảng trắng"
                },
                "onlyAlphaNumeric" : {
                    "regex" : /^[0-9a-zA-Z]+$/,
                    "alertText" : "Chỉ chấp nhận số hoặc ký tự a-z 0-9"
                },
                "onlyMonthYear":{
                    "regex" : /^(0[1-9]|1[012])\/[\d]{4}$/,
                    "alertText" : "Chỉ chấp nhận định dạng mm/yyyy"
                },
                "validContent":{
                    "regex": /^[\da-zA-Z,\@\#\$\!\%\^\&\*\-\_;.\/\(\)+\[\]\: ]+$/,
                    "alertText": "Chỉ chấp nhận các chữ cái, chữ số và các ký tự: & * % $ # @ () , - ; . / \ + [ ] : ^ "
                },
//                ThanhMT bo sung chan cho cac giao dich EFAST, them ky tu ' va "
                "validContent_gdEfast":{                
                	 "regex": /^[\da-zA-Z,\@\#\$\!\%\^\&\*\-\_;.\/\(\)+\[\]\: ]+$/,
                     "alertText": "Chỉ chấp nhận các chữ cái, chữ số và các ký tự: & * % $ # @ () , - ; . / \ + [ ] : ^ "
                	// "regex": /^[\da-zA-Z,\@\#\$\!\%\^\&\*\-\'\"\_;.\/\(\)+\[\] ]+$/,
                    // "alertText": "Chỉ chấp nhận các chữ cái, chữ số và các ký tự: & * % $ # @ () , - ; . / \ + [ ] : ^ "
                    	         
                },
                "validContentWithNewLine":{
                	"regex": /^[\da-zA-Z,\,\'\.\(\)\-\+\/\:\{\}\?\n ]+$/,
                    "alertText": "Chỉ chấp nhận các chữ cái, chữ số và các ký tự: , ' . () - + / : {} ? Giới hạn 35 ký tự/một dòng, tối đa 4 dòng"
                },
                "normalDate":{
                    "regex" : /^(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/][\d]{4}$/,
                    "alertText" : "Chỉ chấp nhận định dạng dd/mm/yyyy"
                },
                "taxDate":{
                	"regex" : /^(0[0-9]|[12][0-9]|3[01])[\/]([[a-zA-z0-9][a-zA-z0-9]]?|00|0[1-9]|1[012])[\/][\d]{4}$/,                    
                    "alertText" : "Chưa đúng định dạng"
                },
                "checkBalance":{
                	"alertText": "Số tiền vượt quá số dư khả dụng!"
                }
            };

        }
    };

    $.validationEngineLanguage.newLang();
})(jQuery);
