/**
 * i18n.js
 *
 * This will setup the i18n language files and locale data for your app.
 *
 *   IMPORTANT: This file is used by the internal build
 *   script `extract-intl`, and must use CommonJS module syntax
 *   You CANNOT use import/export in this file.
 */

import messages_en from './translations/message_en.json';
import messages_vi from './translations/messages_vi.json';
import messages_kr from './translations/messages_kr.json';
import messages_ja from './translations/messages_ja.json';
import messages_ch from './translations/messages_ch.json';

const messages = {
  'en': messages_en,
  'vi': messages_vi,
  'kr': messages_kr,
  'ja': messages_ja,
  'ch': messages_ch
};

const DEFAULT_LOCALE = 'vi';

export {messages,DEFAULT_LOCALE};
