import { createSelector } from 'reselect';
import { initialState } from './BulkPaymentReducer';

const bulkPayment = state => state.bulkPaymentReducer.bulkPayment || initialState.bulkPayment;


const getBulkPayment = createSelector(
    [ bulkPayment],
    (bulkPayment ) => bulkPayment
)

export default getBulkPayment;