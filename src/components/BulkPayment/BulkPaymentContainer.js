import React, { useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actBulkPayment } from './BulkPaymentAction';
import getBulkPayment from './BulkPaymentSelector';
import { runSaga } from '../../common/Store/Store';
import bulkPaymentSaga from './BulkPaymentSaga';

runSaga(bulkPaymentSaga);

const BulkPaymentContainer = () => {
    const dispatch = useDispatch();
    const bulkPaymentState = useSelector(getBulkPayment);

    function onBulkPayment(id){
        dispatch(actBulkPayment({ id: id }));
    }

    return (
        <>
            <p>Xin chào đây là chức năng { bulkPaymentState }</p>
        </>
    );
}
export { BulkPaymentContainer };