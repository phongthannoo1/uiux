import * as types from './BulkPaymentConstants';
import produce from 'immer';

export const initialState = {
    bulkPayment: "BulkPayment"
};

const bulkPaymentReducer = (state = initialState, action) => 
    produce(state , draft =>{
        switch (action.type) {
            case types.BULKPAYMENT:
                draft.bulkPayment = action.payload;
                break;
            
            default: 
            break;
        }
    });
export default bulkPaymentReducer;