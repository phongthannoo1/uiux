import { Fragment } from 'react';

import {Container, Row, Col} from 'react-bootstrap';
import { AppHeader } from '../../common/Layout/AppHeader';
import {NavBar} from '../../common/Layout/AppNavbar';
import { BulkPaymentContainer } from './BulkPaymentContainer';

const BulkPayment = () => {
    return(
        <Fragment>
            <AppHeader />
            <Container fluid="md">
                <Row>
                    <Col xs={3}>
                    <NavBar />
                    </Col>
                    <Col xs={9}>
                    <BulkPaymentContainer />
                    </Col>
                </Row>
            </Container>
        </Fragment>
    );
};

export default BulkPayment;