import { takeEvery, all, put } from 'redux-saga/effects';
import * as types from './BulkPaymentConstants';

function* watchFetchBulkPaymentAction({ payload }) {
    yield console.log(payload);
    // const list = yield select(state);
}

export default function* bulkPaymentSaga(){
    yield all([
        watchFetchBulkPaymentAction()
    ]);  
}