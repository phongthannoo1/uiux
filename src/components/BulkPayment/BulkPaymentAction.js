import * as types from './BulkPaymentConstants';

export const actBulkPayment = payload => {
    return {
        type: types.BULKPAYMENT,
        payload: payload
    }
}