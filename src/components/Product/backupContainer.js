import React, { useState} from 'react';
import { useSelector, useDispatch, connect } from 'react-redux';
import ListProduct from './ListProduct';
import ProductDetail from './ProductDetail';
import { actDecrement, actIncrement, actMulti, changeNumber } from './Action';
import calculate from './Selector';
import { runSaga } from '../../common/Store/Store';
import productSaga from './saga';
import { Input } from '@material-ui/core';

runSaga(productSaga);

// function ProductContainer () {
    
export const ProductContainer = ({ products, onIncrement, onDecrement,numProduct,handleChange }) => {
    
    // const [inputs, setInputs] = useState({
    //     number : ''
    // });

    // const products = useSelector(state => state.productReducer.listProd);
    // const numProduct = useSelector(state => calculate(state));
    // // const inputProd =  useSelector(state => state.productReducer.input);
    // // const numProduct = calculate(state);
    // const dispatch = useDispatch();

    // useEffect(() => {
    //      dispatch(userActions.requestGetAllUser());
    // }, [])

    // function onIncrement(id){
    //     dispatch(actIncrement({ id: id }));
    //     dispatch(actMulti({ id: id }));
    // }

    // function onDecrement(id){
    //     dispatch(actDecrement({ id: id }));
    // }

    // function handleChange(e){
    //     const { name, value } = e.target;
    //     setInputs(inputs => ({ ...inputs, [name]: value }));
    //     dispatch(changeNumber({number:value}));
    // }

    //use not hooks
    // function handleChange(e){
    //     const { name, value } = e.target;
    //     setInputs(inputs => ({ ...inputs, [name]: value }));
    //     dispatch(changeNumber({number:value}));
    // }

    const showProduct = products => {
        console.log(products);
        let result = null;
        if (products.length > 0) {
            result = products.map(product =>
                <ProductDetail key={product.id}
                    product={product}
                    onIncrement={onIncrement}
                    onDecrement={onDecrement}
                />);
        }
        return result;
    };

    return (
        <ListProduct>
            <h1>numProduct la : {numProduct} {process.env.USERNAME}</h1>

            <p>Số lượng nhập:</p>
            {/* <Input type="number" name="number" value={number} onChange={handleChange}/> */}

            {showProduct(products)}
        </ListProduct>
    );
}

const mapStateToProps = state => {
    console.log(state);
    return {
        number: state.productReducer.numInput,
        products: state.productReducer.listProd,
        numProduct : calculate(state)
        // numProduct : initialState.numAdd
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onIncrement: id => {
            dispatch(actIncrement({ id: id }));
            dispatch(actMulti({ id: id }));
        },
        onDecrement: id => {
            dispatch(actDecrement({ id: id }));
        },
        handleChange: number => {
            dispatch(changeNumber({number:number}));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductContainer);
// export { ProductContainer };

