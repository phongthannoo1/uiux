import * as types from './constant';

export const actIncrement = id => {
    return {
        type: types.INCREMENT,
        payload: id
    }
}

export const actDecrement = id => {
    return {
        type: types.DECREMENT,
        payload: id
    }
}

export const actMulti = id => {
    return {
        type: types.MULTI,
        payload: id
    }
}

export const change = id => {
    return {
        type: types.CHANGE,
        payload: id
    }
}

export const changeNumber = number => {
    return {
        type: types.CHANGENUMBER,
        payload: number
    }
}