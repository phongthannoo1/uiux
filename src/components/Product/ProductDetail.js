import React from 'react';

function ProductDetail({ product, onIncrement, onDecrement }) {
    const increment = id => {
        onIncrement(id);
    }

    const decrement = id => {
        onDecrement(id);
    }

    return (
        <div>
            <p>Id: {product.id}</p>
            <p>Name: {product.name}</p>
            <p>Quantity: {product.quantity}</p>
            <button onClick={() => increment(product.id)}>Tang</button>
            <button onClick={() => decrement(product.id)}>Giam</button>
            <hr />
        </div>
    );
}

export default ProductDetail;
