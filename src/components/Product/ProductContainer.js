import React, { useMemo, useState} from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import ListProduct from './ListProduct';
import ProductDetail from './ProductDetail';
import { actDecrement, actIncrement, actMulti, changeNumber } from './Action';
// import calculate from './Selector';
import { runSaga } from '../../common/Store/Store';
import productSaga from './saga';
import { Input } from '@material-ui/core';

import { createSelector } from 'reselect';

const numInput = state => state.productReducer.numInput;


const calculate = createSelector(
    [numInput],
    (num) => {
        return num;
    }
    
)

runSaga(productSaga);

const ProductContainer = () => {
    
    const [inputs, setInputs] = useState({
        number : ''
    });

    const products = useSelector(state => state.productReducer.listProd);
    const numProduct = useSelector(calculate);
    const dispatch = useDispatch();

    function onIncrement(id){
        dispatch(actIncrement({ id: id }));
        dispatch(actMulti({ id: id }));
    }

    function onDecrement(id){
        dispatch(actDecrement({ id: id }));
    }

    function handleChange(e){
        const { name, value } = e.target;
        setInputs(inputs => ({ ...inputs, [name]: value }));
        dispatch(changeNumber({number:value}));
    }

    const showProduct = products => {
        let result = null;
        if (products.length > 0) {
            result = products.map(product =>
                <ProductDetail key={product.id}
                    product={product}
                    onIncrement={onIncrement}
                    onDecrement={onDecrement}
                />);
        }
        return result;
    };

    return (
        <ListProduct>
            <h1>numProduct la : {numProduct} {process.env.REACT_APP_USERNAME}</h1>

            <p>Số lượng nhập:</p>
            <Input type="number" name="number" value={inputs.number} onChange={handleChange}/>

            {showProduct(products)}
        </ListProduct>
    );
}
export { ProductContainer };