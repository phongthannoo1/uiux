import { takeEvery, all, put } from 'redux-saga/effects';
import * as types from './constant';

function* watchFetchProductAction({ payload }) {
    // yield console.log(payload);
    // const list = yield select(state);
}

function* multiProduct({ payload }) {
    yield put({ type: types.INCREMENT, payload });
}

function* changeNumAdd({ payload }) {
    yield put({ type: types.CHANGE, payload });
}

function* sagaWatch() {
    yield takeEvery(types.INCREMENT, watchFetchProductAction);
}

function* sagaMulti() {
    yield takeEvery(types.MULTI, multiProduct);
}

function* sagaChange() {
    yield takeEvery(types.DECREMENT, changeNumAdd);
}

export default function* productSaga() {
    yield all([
        sagaWatch(),
        sagaMulti(),
        sagaChange()
    ]);  
}