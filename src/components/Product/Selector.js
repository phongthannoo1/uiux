import { createSelector } from 'reselect';
import { initialState } from './reducer';

const homeState = state => state.productReducer.numprod || initialState.numprod;

const addState = state => state.productReducer.numAdd || initialState.numAdd;

const numInput = state => state.productReducer.numInput || initialState.numInput;


const calculate = createSelector(
    [ homeState , addState, numInput],
    (homestate, addstate, numInput ) => {
        console.log('useSelector:'+numInput);  
        console.log(homestate);
        console.log(addstate);
        if(numInput < 5){
            return homestate + addstate*2;
        }  else {
            return homestate + addstate;
        }
        
    }
    
)

export default calculate;