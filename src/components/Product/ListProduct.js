import React from 'react';
import { NavLink } from 'react-router-dom';

export const ListProduct = (props) => {
    return (
        <div>
            <h1>Danh sach san pham</h1>
            <div>
                {props.children}
            </div>
            <NavLink exact to="/">Home</NavLink>
            <NavLink exact to="/login">Login</NavLink>
        </div>
    );
}

export default ListProduct;

