import * as types from './constant';
import produce from 'immer';

export const initialState = {
    numprod: 0,
    numAdd : 10,
    numInput: 0,
    listProd:[
    {
        id: 1,
        name: 'iphone 7 plus',
        quantity: 2
    },
    {
        id: 2,
        name: 'sam sung galaxy',
        quantity: 6
    },
    {
        id: 3,
        name: 'xiaomi red 10',
        quantity: 8
    },
    {
        id:10,
        name : 'macbook pro',
        quantity : 2
    }
]};

const productReducer = (state = initialState, action) => 
    produce(state , draft =>{
        switch (action.type) {
            case types.INCREMENT:
                // initialState.listProd.find(x => x.id === payload.id).quantity += 1;
                // return [...state];
                draft.listProd.find(x => x.id === action.payload.id).quantity += 1;
                break;
            case types.DECREMENT:
                // initialState.listProd.find(x => x.id === payload.id).quantity -= 1;
                // return [...state];
                draft.listProd.find(x => x.id === action.payload.id).quantity -= 1;
                break;
            case types.MULTI:
                // initialState.listProd.find(x => x.id === 10).quantity *= 2;
                // return [...state];
                draft.listProd.find(x => x.id === 10).quantity *= 2;
                break;
            case types.CHANGE:
                // if(initialState.numAdd<100){
                //     initialState.numAdd += payload.id;
                //     initialState.numprod += 1;
                // }else{
                //     initialState.numAdd = payload.id;
                // }
                // return [...state];

                if(draft.numAdd<100){
                    draft.numAdd += action.payload.id;
                    draft.numprod += 1;
                }else{
                    draft.numAdd = action.payload.id;
                }
                break;
            case types.CHANGENUMBER:
                draft.numInput = action.payload.number;
                break;
            default: 
            // return [...state.listProd];
            break;
        }
    });
export default productReducer;