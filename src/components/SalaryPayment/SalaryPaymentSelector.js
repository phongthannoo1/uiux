import { createSelector } from 'reselect';
import { initialState } from './SalaryPaymentReducer';

const salaryPayment = state => state.salaryPaymentReducer.salaryPayment || initialState.salaryPayment;


const getSalaryPayment = createSelector(
    [ salaryPayment],
    (salaryPayment ) => salaryPayment
)

export default getSalaryPayment;