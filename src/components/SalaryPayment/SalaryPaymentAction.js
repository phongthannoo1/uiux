import * as types from './SalaryPaymentConstants';

export const actSalaryPayment = payload => {
    return {
        type: types.SALARYPAYMENT,
        payload: payload
    }
}