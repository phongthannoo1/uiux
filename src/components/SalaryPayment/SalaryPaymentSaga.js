import { takeEvery, all, put } from 'redux-saga/effects';
import * as types from './SalaryPaymentConstants';

function* watchFetchSalaryPaymentAction({ payload }) {
    yield console.log(payload);
    // const list = yield select(state);
}

export default function* salaryPaymentSaga(){
    yield all([
        watchFetchSalaryPaymentAction()
    ]);  
}