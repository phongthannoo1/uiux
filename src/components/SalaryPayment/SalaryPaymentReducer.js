import * as types from './SalaryPaymentConstants';
import produce from 'immer';

export const initialState = {
    salaryPayment: "SalaryPayment"
};

const salaryPaymentReducer = (state = initialState, action) => 
    produce(state , draft =>{
        switch (action.type) {
            case types.SALARYPAYMENT:
                draft.salaryPayment = action.payload;
                break;
            
            default: 
            break;
        }
    });
export default salaryPaymentReducer;