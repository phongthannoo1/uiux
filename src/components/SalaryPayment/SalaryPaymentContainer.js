import React, { useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actSalaryPayment } from './SalaryPaymentAction';
import getSalaryPayment from './SalaryPaymentSelector';
import { runSaga } from '../../common/Store/Store';
import salaryPaymentSaga from './SalaryPaymentSaga';

runSaga(salaryPaymentSaga);

const SalaryPaymentContainer = () => {
    const dispatch = useDispatch();
    const salaryPaymentState = useSelector(getSalaryPayment);

    function onSalaryPayment(id){
        dispatch(actSalaryPayment({ id: id }));
    }

    return (
        <>
            <p>Xin chào đây là chức năng { salaryPaymentState }</p>
        </>
    );
}
export { SalaryPaymentContainer };