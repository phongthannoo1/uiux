import * as types from './PaymentInvoiceConstants';
import produce from 'immer';

export const initialState = {
    paymentInvoice: "PaymentInvoice"
};

const paymentInvoiceReducer = (state = initialState, action) => 
    produce(state , draft =>{
        switch (action.type) {
            case types.PAYMENTINVOICE:
                draft.paymentInvoice = action.payload;
                break;
            
            default: 
            break;
        }
    });
export default paymentInvoiceReducer;