import * as types from './PaymentInvoiceConstants';

export const actPaymentInvoice = payload => {
    return {
        type: types.PAYMENTINVOICE,
        payload: payload
    }
}