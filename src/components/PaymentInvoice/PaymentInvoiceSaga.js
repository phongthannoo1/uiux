import { takeEvery, all, put } from 'redux-saga/effects';
import * as types from './PaymentInvoiceConstants';

function* watchFetchPaymentInvoiceAction({ payload }) {
    yield console.log(payload);
    // const list = yield select(state);
}

export default function* paymentInvoiceSaga(){
    yield all([
        watchFetchPaymentInvoiceAction()
    ]);  
}