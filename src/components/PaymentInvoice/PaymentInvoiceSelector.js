import { createSelector } from 'reselect';
import { initialState } from './PaymentInvoiceReducer';

const paymentInvoice = state => state.paymentInvoiceReducer.paymentInvoice || initialState.paymentInvoice;


const getPaymentInvoice = createSelector(
    [ paymentInvoice],
    (paymentInvoice ) => paymentInvoice
)

export default getPaymentInvoice;