import React, { useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actPaymentInvoice } from './PaymentInvoiceAction';
import getPaymentInvoice from './PaymentInvoiceSelector';
import { runSaga } from '../../common/Store/Store';
import paymentInvoiceSaga from './PaymentInvoiceSaga';

runSaga(paymentInvoiceSaga);

const PaymentInvoiceContainer = () => {
    const dispatch = useDispatch();
    const paymentInvoiceState = useSelector(getPaymentInvoice);

    function onPaymentInvoice(id){
        dispatch(actPaymentInvoice({ id: id }));
    }

    return (
        <>
            <p>Xin chào đây là chức năng { paymentInvoiceState }</p>
        </>
    );
}
export { PaymentInvoiceContainer };