/**
 * LanguageProvider
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { IntlProvider } from 'react-intl';

import { makeSelectLocale } from './selectors';
import { useSelector } from 'react-redux';
import { messages } from '../../i18n';

export function LanguageProvider(props) {
    const locale = useSelector(state=>state.language.locale);
  return (
    <IntlProvider
      locale={locale}
      defaultLocale="vi"
      messages={messages[locale]}
    >
      {React.Children.only(props.children)}
    </IntlProvider>
  );
}