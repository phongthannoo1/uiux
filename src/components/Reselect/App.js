import React,{Fragment, useState} from 'react'
import {NumberList} from './VisibleTodoList';
import { toggleTodo, triggerTodo } from './Action';
import { useDispatch, useSelector } from 'react-redux';
import { Input } from '@material-ui/core';
 
export const App = () => {
  const initNumber = useSelector(state=>state.number);
  const [inputs, setInputs] = useState({
      number : initNumber
  });
  const dispatch = useDispatch();

  function handleChange(e){
      const { name, value } = e.target;
      setInputs(inputs => ({ ...inputs, [name]: value }));
      dispatch(triggerTodo(e.target.value));
  }
  
  return (
    <Fragment >
        <p>Số lượng nhập:</p>
        <Input type="number" name="number" value={inputs.number} onChange={handleChange}/>
        <NumberList />
    </Fragment>
)}

// export {App};