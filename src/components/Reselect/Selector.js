import { createSelector } from 'reselect';

export const selectUsers = state => state.reselect.number;

const selectNumOfUsers = createSelector(
  [selectUsers],
  number =>number
);

export default selectNumOfUsers;