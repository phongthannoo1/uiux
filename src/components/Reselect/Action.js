
export const toggleTodo = id => {
    return {
        type: 'toggleTodo',
        payload: id
    }
}

export const triggerTodo = id => {
    return {
        type: 'triggerTodo',
        payload: id
    }
}