import produce from 'immer';

export const initialState = {
    triggerNumber:0,
    number: 55
};

const reselectReducer = (state = initialState, action) => 
    produce(state , draft =>{
        switch (action.type) {
            case 'toggleTodo':
                draft.number = action.payload;
                break;
            case 'triggerTodo':
                if(action.payload>5){
                    draft.number += action.payload;
                }
                draft.triggerNumber = action.payload;                
                break;
            default: 
            break;
        }
    });
export default reselectReducer;