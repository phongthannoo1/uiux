import React from 'react';
import { useSelector, connect } from 'react-redux';
import selectNumOfUsers from './Selector';
// import { createSelector } from 'reselect';

// const selectUsers = state => state.reselect.number;

// const selectNumOfUsers = createSelector(
//   [selectUsers],
//   number => {console.log('reselect'); return number}
// );

export const UsersCounter = () => {
  const NumOfUsers = useSelector(selectNumOfUsers);
  console.log('rerender');
  return <div>{NumOfUsers}</div>
};

const NumberList = () => {
  return (
    <>
      <span>Number of users:</span>
      <UsersCounter />
    </>
  )
};

const mapState = (state) => {
  return {
    NumOfUsers: state.reselect.number
  }
}

// export {NumberList};
const Nume =  connect(mapState)(NumberList);

export {Nume as NumberList};