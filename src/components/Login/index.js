import React , {useEffect, useState} from 'react';
// import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { userActions } from '../../common/Actions/user.actions';


import Avatar from '@material-ui/core/Avatar';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
// import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useLocation } from 'react-router-dom';
import { runSaga } from './../../common/Store/Store';
import rootSaga from './../../common/Saga/user.saga';

runSaga(rootSaga);

function Copyright() {
    
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://material-ui.com/">
          Your Website
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }

  export function LoginPage () {
    const [inputs, setInputs] = useState({
      userName : '',
      password : ''
    });

    const [submitted, setSubmitted] =  useState(false);
    const {userName , password} = inputs;
    const dispatch = useDispatch();
    const loggingIn = useSelector(state => state.authentication.loggingIn);
    const location = useLocation();

    useEffect(() => {
        dispatch(userActions.logout());
    }, []);

    function handleChange(e){
        const { name, value } = e.target;
        setInputs(inputs => ({ ...inputs, [name]: value }));
    }

    function handleSubmit(e){
        e.preventDefault();
        setSubmitted(true);
        if(userName && password) {
          const {from} = location.state || {from : {pathname:'/'}};
          dispatch(userActions.requestLogin({userName, password, from}));
        }
    }

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className="paper">
            <Avatar className="avatar">
            <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
            Sign in
            </Typography>
            <form className="form" noValidate onSubmit={handleSubmit}>
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="userName"
                autoComplete="email"
                autoFocus
                value={userName} onChange={handleChange}
            />
            {submitted && !userName &&
                <Alert severity="error">Username is required</Alert>
            }
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password} onChange={handleChange}
            />
            {submitted && !password &&
                <Alert severity="error">Password is required</Alert>
            }
            {submitted && password && userName &&
                <Alert severity="error">
                    Đăng nhập thất bại
                </Alert>
            }
            <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
            />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className="submit"
            >
                Sign In
            </Button>
            <Grid container>
                <Grid item xs>
                <Link href="#" variant="body2">
                    Forgot password?
                </Link>
                </Grid>
                <Grid item>
                <Link href="#" variant="body2">
                    {"Don't have an account? Sign Up"}
                </Link>
                </Grid>
            </Grid>
            </form>
        </div>
        <Box mt={8}>
            <Copyright />
        </Box>
        </Container>
    );
}

export default LoginPage;