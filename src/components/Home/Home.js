import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { userActions } from '../../common/Actions/user.actions';
// import localStorageService from '../../auth/localStorageService';
import CryptoJS from 'crypto-js';
import JSEncrypt from 'jsencrypt';
import axios from 'axios';
import { runSaga } from './../../common/Store/Store';
import rootSaga from './../../common/Saga/user.saga';
import { FormattedMessage } from 'react-intl';

runSaga(rootSaga);


function HomePage() {
    // localStorageService().setitem
    const userEncry = { userId: 'd41cd772-cb57-412c-a864-6e40b2bd3e12' };
    const stateEncy = { type: 'payment', amount: 100, number: '123-456-789-000', securitycode: '123' ,log:''};
    const users = useSelector(state => state.users);
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();

    useEffect(() => {
         dispatch(userActions.requestGetAllUser());
    }, [])

    function handleDeleteUser(id){
        console.log(user);
        console.log(userEncry);
        dispatch(userActions.requestDelete(id))
    }

    function doTransaction() {
        // this.log("RSA public key[base64]: " +  resUser.data.rsaPublicKey);

        let transaction = {
             type: stateEncy.type, amount: stateEncy.amount, 
             creditcard: { number: stateEncy.number, securitycode: stateEncy.securitycode } };

        //generate AES key
        var secretPhrase = CryptoJS.lib.WordArray.random(16);
        var salt = CryptoJS.lib.WordArray.random(128 / 8);
        //aes key 128 bits (16 bytes) long
        var aesKey = CryptoJS.PBKDF2(secretPhrase.toString(), salt, {
            keySize: 128 / 32
        });
        //initialization vector - 1st 16 chars of userId
        var iv = CryptoJS.enc.Utf8.parse("anh.phammai");
        var aesOptions = { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: iv };
        var aesEncTrans = CryptoJS.AES.encrypt(JSON.stringify(transaction), aesKey, aesOptions);

        console.log(`Transaction: ${JSON.stringify(transaction)}`);
        console.log('AES encrypted transaction [Base64]: ' + aesEncTrans.toString());
        console.log('AES key [hex]: ' + aesEncTrans.key);
        console.log('AES init vector [hex]: ' + aesEncTrans.iv);

        //encrypt AES key with RSA public key
        var rsaEncrypt = new JSEncrypt();
        // rsaEncrypt.setPublicKey(resUser.data.rsaPublicKey);
        var rsaEncryptedAesKey = rsaEncrypt.encrypt(aesEncTrans.key.toString());
        console.log('RSA encrypted AES key [base64]: ' + rsaEncryptedAesKey);

        var encryptedTransaction = { userId: userEncry.userId, fullName: aesEncTrans.toString(), jobTitle: rsaEncryptedAesKey };

        // doTransaction(encryptedTransaction);

        axios.post('http://192.168.70.173:8080/filtered/hello',encryptedTransaction)
        .then(resp =>{
            console.log(resp);
        });
        // showInfoMessage(this, 'System', 'Secure transaction completed.');
   
    }

    return (
        <div className="col-md-6 col-md-offset-3">
            <h1>Hi {user.userName}!</h1>
            <p>You're logged in with React!!Version1  sdsd</p>
            <h3>All registered users:</h3>
            {users.loading && <em>Loading users...</em>}
            {users.error && <span className="text-danger">ERROR: {users.error}</span>}
            {users.items &&
                <ul>
                    {users.items.map((user, index) =>
                        <li key={user.id}>
                            {user.username + ' ' + user.role}
                            {
                                user.deleting ? <em> - Deleting...</em>
                                : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                                : <span> - <button onClick={() => handleDeleteUser(user.id)}>Delete</button></span>
                            }
                        </li>
                    )}
                </ul>
            }
            <button onClick={() => doTransaction()}><FormattedMessage id="doTransaction"/></button>
            <p>
                <Link to="/product">Product</Link>
            </p>
            <p>
                <Link to="/login">Logout</Link>
            </p>
        </div>
    );
}

export {HomePage};