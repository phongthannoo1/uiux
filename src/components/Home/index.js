import { Fragment } from 'react';
import { HomePage } from './Home';

import {Container, Row, Col} from 'react-bootstrap';
import {NavBar} from '../../common/Layout/AppNavbar';
import { AppHeader } from '../../common/Layout/AppHeader';

const HomeIndex = () => {
    return(
        <Fragment>
            <AppHeader />
                <Container fluid="md">
                <Row>
                    <Col xs={3}>
                        <NavBar/>
                    </Col>
                    <Col xs={9}>
                        <HomePage />
                    </Col>
                </Row>                
            </Container>
        </Fragment>
    );
};

export default HomeIndex;