import React, { useEffect, useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actSocialInsurance } from './SocialInsuranceAction';
import getSocialInsurance from './SocialInsuranceSelector';
import { runSaga } from '../../common/Store/Store';
import socialInsuranceSaga from './SocialInsuranceSaga';
import { AccountList } from '../../common/Component/GetAccountList';
import Axios from 'axios';
import { ProvinceList } from './GetProvinces';
import { doGet, doPost } from '../../common/API/BaseApi';
import { SiOrgList } from './GetSiOrg';
import { isEmpty } from 'lodash';
import { Autocomplete } from '@material-ui/lab';
import { TextField } from '@material-ui/core';
import { decrypt } from '../../common/Utils/Crypto';
// import './style.scss';

runSaga(socialInsuranceSaga);

const SocialInsuranceContainer = () => {
    const dispatch = useDispatch();
    const socialInsuranceState = useSelector(getSocialInsurance);
    const data = {
        "channel":	"eFAST",
        "cifno":	"300113679",
        "language":	"vi" ,
        "newCore":	"Y" ,	
        "requestId"	:"AFA79B983E" ,
        "serviceCode": "SIPayment",
        "username":	"ngoc.ntm1" ,
        "version"	:"1.0"
    }
    
    const [listAccount, setListAccount] =  useState([]) ;
    const [listProvince, setListProvince] =  useState([]) ;
    const [listSiOrg, setListSiOrg] =  useState([]) ;
    const [listContact, setListContact] =  useState([]) ;
    const [account,setAccount] =  useState(null) ;
    const [province,setProvince] =  useState(null) ;
    const [siOrg,setSiOrg] =  useState(null) ;
    const [branchID,setBranchID] =  useState(null) ;
    const [siId,setSiId] =  useState(null) ;

    const dataRequestSiOrg={
        "channel":	"eFAST",
        "cifno":	"300113679",
        "language":	"vi" ,
        "newCore":	"Y" ,	
        "requestId"	:"AFA79B983E" ,
        "username":	"ngoc.ntm1" ,
        "version"	:"1.0",
        "provinceID":province,
        "branchID":branchID,
        "ipAddress":"10.65.48.108"
    }

    const dataRequestContact={
        "channel":	"eFAST",
        "cifno":	"300113679",
        "language":	"vi" ,
        "newCore":	"Y" ,	
        "requestId"	:"AFA79B983E" ,
        "username":	"ngoc.ntm1" ,
        "version"	:"1.0",
        "serviceId":"sip",
    }

    function onSocialInsurance(id){
        dispatch(actSocialInsurance({ id: id }));
    }
    
    useEffect( () => {
        //get accountIist
        doPost('/account/list',data,(res,err)=>{
            if(!isEmpty(res)){
                console.log(res);
                setListAccount(res.data.accounts);
                console.log(listAccount);
            }else if(!isEmpty(err)){
                console.log(err);
            }
        })
        //get provinceList
        doGet('/socialinsurance/getprovince',(res,err)=>{
            if(!isEmpty(res)){
                console.log(res);
                setListProvince(res.data.lstProvince);
                console.log(listProvince);
            }else if(!isEmpty(err)){
                console.log(err);
            }            
        })
        //get contact list
        doPost('/contact/list',dataRequestContact,(res,err)=>{
            console.log("contact");
            if(!isEmpty(res)){
                console.log(res);
                setListContact(res.data.contacts);
                console.log(listContact);
            }else if(!isEmpty(err)){
                console.log(err);
            }
        })
    }, []); 

    useEffect( () => {
        if(province!==null&&province!==""){
            doPost('/socialinsurance/getorg',dataRequestSiOrg,(res,err)=>{
                console.log(res);
                setListSiOrg(res.data.lstOrgInfo);
                console.log(listAccount);
            })
        }  else{
            setListSiOrg([]);
        }      
    }, [province]); 

    const dataExport={
        "username":"thu.nh",
        "trantype":"INS",
        "fromdate":"01-12-2020",
        "todate":"28-12-2020",
        "status":"00",
        "fromPage":1,
        "toPage":10,
        "cifno":"300113679",
        "dateType":"1",
        "exportType":"PDF"
    }

    // function downloadExcel(){
    //     doPost('/report/export',dataExport,(res,err)=>{
    //         if(res){
    //             console.log(res);
    //             const url = window.URL.createObjectURL(new Blob([res.data]));
    //             const link = document.createElement('a');
    //             link.href = url;
    //             link.setAttribute('download', 'file.pdf'); //or any other extension
    //             document.body.appendChild(link);
    //             link.click();
    //         }            
    //     },[],{"responseType": 'blob'})
    //     Axios.post('http://localhost:8092/report/export',dataExport,{responseType: 'blob'})
    //     .then((response) => {
    //         console.log(response);
    //         const url = window.URL.createObjectURL(new Blob([response.data]));
    //         const link = document.createElement('a');
    //         link.href = url;
    //         link.setAttribute('download', 'file.pdf'); //or any other extension
    //         document.body.appendChild(link);
    //         link.click();
    //      });
    // }

    function decode(value){
        var decoded = decrypt(value);
        console.log(decoded);
    }

    function doCheck(){
        console.log(account);
        console.log(siId);
        console.log(siOrg);
        console.log(branchID);
        console.log(province);
    }



    return (
        <>
            <p>Xin chào đây là chức năng { socialInsuranceState }</p>
            <h1 className="page-title">Nộp BHXH</h1>
            <div className="page-content">
            <ul className="page-step clearfix">
                    <li className="current">1. Nhập thông tin </li>
                    <li>2. Xác nhận</li>
                    <li>3. Kết quả</li>
            </ul>
            <form id="socialinsuranceForm" name="socialinsuranceForm" className="form-horizontal clearfix" role="form" action="/payment/social-insurance.do" method="post">
                <div className="form-body">
                    
            <h3 className="action-title">Thông tin tài khoản nộp</h3>    
            <div className="action-box">
                    <div className="form-group">
                        <label className="col-md-4 control-label">Tài khoản nguồn(*)</label>
                        <div className="col-md-8">
                            <select id="fromAccountNo" name="fromAccountNo" className="form-control validate[required]" onChange={(e) => {setAccount(e.target.value); decode(e.target.value)}}>
                                
                                <AccountList listAccount={listAccount} account={account} />
                                {/* {showAccount(listAccount)} */}
                            </select>
                        </div>

                        {/* <Autocomplete
                        id="combo-box-demo"
                        options={listAccount}
                        getOptionLabel={(options) =>  options.accountNo +"-"+ options.currency +"-"+ options.availableBalance +"-"+ options.aliasName}
                        style={{ width: 300 }}
                        renderInput={(params) => <TextField {...params} label="Combo box" variant="outlined" />}
                        /> */}
                        
                        {/* <button name="btn" type="button" onClick={downloadExcel} className="btn blue">Download</button> */}
                    </div>
                </div>
                <h3 className="action-title">Thông tin BHXH</h3>
                <div className="action-box">
                    <div className="form-group">
                        <label className="col-md-4 control-label">Mã địa bàn hành chính(*)</label>
                        <div className="col-md-8">
                                <select id="provinceId" name="provinceId" onChange={(e) => setProvince(e.target.value)} className="form-control validate[required] select2-hidden-accessible">
                                
                                <ProvinceList listProvince={listProvince} />
                                
                                </select>
                                {/* <Autocomplete
                                id="combo-box-demo"
                                options={listProvince}
                                getOptionLabel={(options) =>  options.provinceID +"-"+ options.provinceName }
                                style={{ color: 'white' }}
                                renderInput={(params) => <TextField {...params} label="Combo box" variant="outlined" />}
                                /> */}
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-md-4 control-label">Mã cơ quan BHXH(*)</label>
                        <div className="col-md-8">	                
                                <select id="siOrgCode" name="siOrgCode" onChange={(e) => setSiOrg(e.target.value)} className="form-control validate[required] select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                
                                <SiOrgList listSiOrg={listSiOrg} />
                                </select>
                        </div> 
                    </div>             
                    <div className="form-group">
                        <label className="col-md-4 control-label">Mã đối tượng BHXH(*)</label>
                        <div className="col-md-8">
                            <div className="input-icon right">
                            <a onclick="">
                                <i className="icon-search"></i>
                            </a>
                            <input className="form-control validate[required]" maxlength="35" placeholder="Nhập đối tượng BHXH" type="text" value={siId} onChange={(e) => setSiId(e.target.value)} autocomplete="off"/>
                            
                            {/* <Autocomplete
                                id="combo-box-demo"
                                options={listContact}
                                getOptionLabel={(options) =>  options.customercode +"-"+ options.payeename}
                                // style={{ color: 'white' , 
                                // fontFamily : "Tahoma",
                                // fontSize: "14px",
                                // display: "block",
                                // width: "100%",
                                // padding: "6px 6px",
                                // lineHeight: "1.428571429",
                                // color: "#214b85",
                                // verticalAlign: "middle",
                                // backgroundColor: "#ffffff",
                                // border: "1px solid #cccccc"}}
                                renderInput={(params) => (
                                    <div ref={params.InputProps.ref}>
                                        <a onclick="">
                                            <i className="icon-search"></i>
                                        </a>
                                        <input {...params.inputProps} className="form-control validate[required]" 
                                        onChange={(e) => {console.log(e.target.value);}} 
                                        placeholder="Nhập đối tượng BHXH" type="text" autocomplete="off"/>
                                    </div>
                                )}
                                /> */}
                            </div>
                        </div>   
                    </div>
                    <div id="loadingDiv" ></div>
                    <div className="form-actions right">
                        <button name="btn" type="button" onClick={doCheck} className="btn blue">
                            Kiểm tra
                            <i className="icon-double-angle-right"></i>
                        </button>
                    </div>
                    <h3 className="action-title">
                        Chi tiết giao dịch
                    </h3>

                    <div className="action-box">
                        <div className="form-group">
                            <label className="col-md-4 control-label" >Số tiền phải nộp </label>
                            <div className="input-currency col-md-8">
                                <input id="amountsPayable" name="amountsPayable" maxlength="17" type="text" className="amountUSD form-control validate[required]" value="" readonly="" /><b><span className="cur"></span></b>
                                <input type="hidden" value="" id="amountPayable" name="amountPayable" />
                                
                            </div>
                        </div>
                
                        <div className="form-group">
                            <label className="col-md-4 control-label" >Số tiền thanh toán </label>
                            <div className="input-currency col-md-8">
                                <input id="amounts" name="amounts" maxlength="17" type="text" className="amountUSD form-control validate[required]" onkeyup="dovalidate(this.id)" value="" /><b><span className="cur"></span></b>
                                <input type="hidden" value="" id="amount" name="amount" />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-md-4 control-label">Nội dung giao dịch <span className="help-block"> Còn <input className="small_left" readonly="" type="text" name="remLen2" size="4" maxlength="4" value="146" /> ký tự
                            </span> </label>
                            <div className="col-md-8">
                                <textarea cols="5" id="content" name="content" className="form-control validate[required,custom[validContent_gdEfast]]" onblur="" onkeydown="textCounter(document.socialinsuranceForm.content,document.socialinsuranceForm.remLen2,146)" onkeyup="textCounter(document.socialinsuranceForm.content,document.socialinsuranceForm.remLen2,146); dovalidate(this.id)"></textarea>
                            </div>
                        </div>
                    </div>
            
                <div className="form-actions right">
                    <button name="btn" type="button" onclick="" className="btn default">
                        Xóa
                    </button>  
                    <button name="btn" type="button" onclick="" className="btn blue">
                        Tiếp tục
                        <i className="icon-double-angle-right"></i>
                    </button>    

                    
                </div>
            </div>
            <input type="hidden" name="org.apache.struts.taglib.html.TOKEN" value="0b946554f6dbf624880432eaf62af89f" />
            <input type="hidden" value="" name="ACTION" id="ACTION" />
            <input type="hidden" value="" name="CURR_TYPE" id="CURR_TYPE" />
                <input type="hidden" value="" name="socialInsuranceName" id="socialInsuranceName" />		
                <input type="hidden" value="" name="toAccount" id="toAccount" />	
                <input type="hidden" value="" name="receiverName" id="receiverName" />
                <input type="hidden" value="" name="receiverBank" id="receiverBank" />
                <input type="hidden" id="branchId" name="branchId" />
                <input type="hidden" id="siOrgName" name="siOrgName" value="" />
                <input type="hidden" id="provinceName" name="provinceName" value="" />		
                <input type="hidden" id="isNon" name="isNon" value="true" />
                    
            </div>
            </form>
            </div>
        </>
    );
}
export { SocialInsuranceContainer };