

const ProvinceList = ({listProvince}) => {
       console.log('listProvince');
       console.log(listProvince);
    return (
        <>
            <option selected="selected" value="">-Chọn mã địa bàn hành chính-</option>
            {listProvince.map((province, index) =>
                <option value={province.provinceID} provincename={province.provinceName}>
                    {province.provinceID} - {province.provinceName}
                </option>
            )}
        </>
    )
}

export {ProvinceList};