

const SiOrgList = ({listSiOrg}) => {
       console.log('listSiOrg');
       console.log(listSiOrg);
    return (
        <>
            <option  selected={true} value="">-Chọn mã cơ quan BHXH-</option>
            {listSiOrg.map((province, index) =>
                <option value={province.orgID} orgacct={province.orgAcct} orgacctname={province.orgAcctName}
                orgacctbranch={province.orgAcctBranch}>{province.orgID} - {province.orgAcctName}</option>
            )}
        </>
    )
}

export {SiOrgList};