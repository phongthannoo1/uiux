import * as types from './SocialInsuranceConstants';
import produce from 'immer';

export const initialState = {
    socialInsurance: "SocialInsurance",
    listAccount:[],
    account:"12"
};

const socialInsuranceReducer = (state = initialState, action) => 
    produce(state , draft =>{
        switch (action.type) {
            case types.SOCIALINSURANCE:
                draft.socialInsurance = action.payload;
                break;
            
            default: 
            break;
        }
    });
export default socialInsuranceReducer;