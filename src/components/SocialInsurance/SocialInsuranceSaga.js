import { takeEvery, all, put } from 'redux-saga/effects';
import * as types from './SocialInsuranceConstants';

function* watchFetchSocialInsuranceAction({ payload }) {
    yield console.log(payload);
    // const list = yield select(state);
}

export default function* socialInsuranceSaga(){
    yield all([
        watchFetchSocialInsuranceAction()
    ]);  
}