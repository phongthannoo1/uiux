import * as types from './SocialInsuranceConstants';

export const actSocialInsurance = payload => {
    return {
        type: types.SOCIALINSURANCE,
        payload: payload
    }
}