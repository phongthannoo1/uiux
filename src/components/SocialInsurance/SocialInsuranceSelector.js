import { createSelector } from 'reselect';
import { initialState } from './SocialInsuranceReducer';

const socialInsurance = state => state.socialInsuranceReducer.socialInsurance || initialState.socialInsurance;


const getSocialInsurance = createSelector(
    [ socialInsurance],
    (socialInsurance ) => socialInsurance
)

export default getSocialInsurance;