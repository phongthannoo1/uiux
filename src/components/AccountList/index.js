import { Fragment } from 'react';

import {Container, Row, Col} from 'react-bootstrap';
import { AppHeader } from '../../common/Layout/AppHeader';
import {NavBar} from '../../common/Layout/AppNavbar';
import { AccountListContainer } from './AccountListContainer';

const AccountList = () => {
    return(
        <Fragment>
            <AppHeader />
            <Container fluid="md">
                <Row>
                    <Col xs={3}>
                    <NavBar />
                    </Col>
                    <Col xs={9}>
                    <AccountListContainer />
                    </Col>
                </Row>
            </Container>
        </Fragment>
    );
};

export default AccountList;