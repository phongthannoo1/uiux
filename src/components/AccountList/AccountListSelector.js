import { createSelector } from 'reselect';
import { initialState } from './AccountListReducer';

const accountList = state => state.accountListReducer.accountList || initialState.accountList;


const getAccountList = createSelector(
    [ accountList],
    (accountList ) => accountList
)

export default getAccountList;