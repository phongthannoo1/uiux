import React, { useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { actAccountList } from './AccountListAction';
import getAccountList from './AccountListSelector';
import { runSaga } from '../../common/Store/Store';
import accountListSaga from './AccountListSaga';

runSaga(accountListSaga);

const AccountListContainer = () => {
    const dispatch = useDispatch();
    const accountListState = useSelector(getAccountList);

    function onAccountList(id){
        dispatch(actAccountList({ id: id }));
    }

    return (
        <>
            <p>Xin chào đây là chức năng { accountListState }</p>
        </>
    );
}
export { AccountListContainer };