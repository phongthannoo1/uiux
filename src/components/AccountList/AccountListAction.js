import * as types from './AccountListConstants';

export const actAccountList = payload => {
    return {
        type: types.ACCOUNTLIST,
        payload: payload
    }
}