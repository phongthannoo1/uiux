import { takeEvery, all, put } from 'redux-saga/effects';
import * as types from './AccountListConstants';

function* watchFetchAccountListAction({ payload }) {
    yield console.log(payload);
    // const list = yield select(state);
}

export default function* accountListSaga(){
    yield all([
        watchFetchAccountListAction()
    ]);  
}