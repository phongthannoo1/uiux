import * as types from './AccountListConstants';
import produce from 'immer';

export const initialState = {
    accountList: "AccountList"
};

const accountListReducer = (state = initialState, action) => 
    produce(state , draft =>{
        switch (action.type) {
            case types.ACCOUNTLIST:
                draft.accountList = action.payload;
                break;
            
            default: 
            break;
        }
    });
export default accountListReducer;