import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function checkRole(roles, userRoles){
    let hasNoRole = false;
    roles.forEach(role=>{
        if(userRoles.indexOf(role) === -1){
            return true;
        }        
    })
    return hasNoRole;
}

export const PrivateRoute = ({ component: Component, roles, roleId, ...rest }) => (
    //...rest is path of route
    <Route {...rest} render={props => {
        let currentUser = localStorage.getItem('user');
        currentUser = JSON.parse(currentUser);
        if(!currentUser){
            // not logged in so redirect to login page with the return url
            return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }

        // check if route is restricted by role
        if(roles && checkRole(roles,currentUser.hasRole)){
            console.log(currentUser.roleId !== roleId);
            return <Redirect to={{ pathname: '/access-denied'}} />
        }

        //check maker or approver
        if(roleId && currentUser.roleId !== roleId ){
            console.log(currentUser.roleId !== roleId);
            return <Redirect to={{ pathname: '/access-denied'}} />
        }
        // localStorage.getItem('user')
        //     ? <Component {...props} />
        //     : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />

        return <Component {...props} />
    }} />
)