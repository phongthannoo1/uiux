import React from 'react'
import { Router} from 'react-router-dom';
// import ProductContainer from '../components/Product/index121';
// import {LoginPage} from '../components/Login';
// import {RegisterPage} from '../components/Register';
// import {HomeIndex} from '../components/Home';
// import {PrivateRoute} from './PrivateRoute';
import {history} from  './History';
import { AppMain } from '../common/Layout/AppMain';

const Routers = props => {
    return (
        <Router history={history}>
            <AppMain />
        </Router>
    )
}

export default Routers;
