module.exports = function (plop) {
    // controller generator
    plop.setGenerator('component', {
        description: 'application controller logic',
        prompts: [{
            type: 'input',
            name: 'name',
            message: 'What is your component name?'
        }],
        actions: [{
            type: 'add',
            path: 'src/components/{{pascalCase name}}/index.js',
            templateFile: 'src/components/templates/index.hbs'
        }
        ,{
            type: 'add',
            path: 'src/components/{{pascalCase name}}/{{pascalCase name}}Container.js',
            templateFile: 'src/components/templates/component.hbs'
        },{
            type: 'add',
            path: 'src/components/{{pascalCase name}}/{{pascalCase name}}Reducer.js',
            templateFile: 'src/components/templates/reducer.hbs'
        },{
            type: 'add',
            path: 'src/components/{{pascalCase name}}/{{pascalCase name}}Constants.js',
            templateFile: 'src/components/templates/constants.hbs'
        },{
            type: 'add',
            path: 'src/components/{{pascalCase name}}/{{pascalCase name}}Saga.js',
            templateFile: 'src/components/templates/saga.hbs'
        },{
            type: 'add',
            path: 'src/components/{{pascalCase name}}/{{pascalCase name}}Action.js',
            templateFile: 'src/components/templates/action.hbs'
        },{
            type: 'add',
            path: 'src/components/{{pascalCase name}}/{{pascalCase name}}Selector.js',
            templateFile: 'src/components/templates/selector.hbs'
        }
    ]
    });
};